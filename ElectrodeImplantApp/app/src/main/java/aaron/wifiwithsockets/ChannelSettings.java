package aaron.wifiwithsockets;

import android.content.Context;
import android.util.AttributeSet;

import java.util.ArrayList;

import static android.R.id.list;

/**
 * Created by aarondelle on 5/25/7.
 */

public class ChannelSettings extends android.support.v7.widget.AppCompatButton {

    private int id = 0;
    private boolean selected = false;
    private boolean set = false;
    private int aAmpPosition = 0;
    private int cAmpPosition = 0;
    private int aPhaseWidthPosition = 0;
    private int cPhaseWidthPosition = 0;
    private int startDelayPosition = 0;
    private int interPhaseDelayPosition = 0;
    private boolean reversePolarity = false;
    private boolean chargeCancellation = false;

    public ChannelSettings(Context context) {
        super(context);
    }

    public ChannelSettings(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChannelSettings(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setID(int id) { this.id = id; }

    public int getChannelID() { return id; }

    public void clear() {
        set = false;
        aAmpPosition = 0;
        cAmpPosition = 0;
        aPhaseWidthPosition = 0;
        cPhaseWidthPosition = 0;
        startDelayPosition = 0;
        interPhaseDelayPosition = 0;
        reversePolarity = false;
        chargeCancellation = false;
    }

    public boolean isSelected() { return  selected; }

    public void setSelected(boolean isSelected) { selected = isSelected; }

    public boolean isSet() { return set; }

    public void set(int aAmpPosition, int cAmpPosition, int aPhaseWidthPosition,
                    int cPhaseWidthPosition, int startDelayPosition, int interPhaseDelayPosition,
                    boolean reversePolarity, boolean chargeCancellation) {
        set = true;
        this.setaAmpPosition(aAmpPosition);
        this.setcAmpPosition(cAmpPosition);
        this.setaPhaseWidthPosition(aPhaseWidthPosition);
        this.setcPhaseWidthPosition(cPhaseWidthPosition);
        this.setStartDelayPosition(startDelayPosition);
        this.setInterPhaseDelayPosition(interPhaseDelayPosition);
        this.setReversePolarity(reversePolarity);
        this.setChargeCancellation(chargeCancellation);
    }

    public void set(Preset.Settings setting) {
        set(setting.aAmpPosition, setting.cAmpPosition, setting.aPhaseWidthPosition,
                setting.cPhaseWidthPosition, setting.startDelayPosition, setting.interPhaseDelayPosition,
                setting.reversePolarity, setting.chargeCancellation);
    }

    public boolean isReversePolarity() { return reversePolarity; }

    public void setReversePolarity(boolean isTrue) { reversePolarity = isTrue; }

    public boolean isChargeCancellation() { return chargeCancellation; }

    public void setChargeCancellation(boolean isTrue) { chargeCancellation = isTrue; }

    public int getaAmpPosition() { return aAmpPosition; }

    public void setaAmpPosition(int position) { aAmpPosition = position; }

    public int getcAmpPosition() { return cAmpPosition; }

    public void setcAmpPosition(int position) { cAmpPosition = position; }

    public int getaPhaseWidthPosition() { return aPhaseWidthPosition; }

    public void setaPhaseWidthPosition(int position) { aPhaseWidthPosition = position; }

    public int getcPhaseWidthPosition() { return cPhaseWidthPosition; }

    public void setcPhaseWidthPosition(int position) { cPhaseWidthPosition = position; }

    public int getStartDelayPosition() { return startDelayPosition; }

    public void setStartDelayPosition(int position) { startDelayPosition = position; }

    public int getInterPhaseDelayPosition() { return interPhaseDelayPosition; }

    public void setInterPhaseDelayPosition(int position) { interPhaseDelayPosition = position; }

    public boolean sameSettingsAs(ChannelSettings otherChannel) {
        return
                this.getaAmpPosition() == otherChannel.getaAmpPosition() &&
                        this.getcAmpPosition() == otherChannel.getcAmpPosition() &&
                        this.getaPhaseWidthPosition() == otherChannel.getaPhaseWidthPosition() &&
                        this.getcPhaseWidthPosition() == otherChannel.getcPhaseWidthPosition() &&
                        this.getStartDelayPosition() == otherChannel.getStartDelayPosition() &&
                        this.getInterPhaseDelayPosition() == otherChannel.getInterPhaseDelayPosition() &&
                        this.isReversePolarity() == otherChannel.isReversePolarity() &&
                        this.isChargeCancellation() == otherChannel.isChargeCancellation();
    }

}