package aaron.wifiwithsockets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiEnterpriseConfig;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ConstantConditions")
@SuppressLint("InflateParams")
public class MainActivity extends AppCompatActivity {

    private static final boolean testing = false;                                   //Change from writeUTF() to writeBytes() and change LAB_SSID, LAB_PASS, ipAddress and portNumber
    private final boolean[] expectingReceivingData = {true};
    //WiFi stuff
    public static final String LAB_SSID = testing ? "ASUS" : "CC3200NET";
    public static final String LAB_PASS = testing ? "ibrcnsi6511" : "password1";
    public static String networkSSID = LAB_SSID;
    public static String networkPersonalPassword = LAB_PASS;
    public static String networkEnterpriseUsername = "";
    public static String networkEnterprisePassword = "";
    public String ipAddress = testing ? "192.168.1.108" : "192.168.1.1";
    public int portNumber = testing ? 4444 : 2048;
    private static final int WIFI_TIMEOUT_MILIS = 10000;
    /**
     * 0 for enterprise, 1 for personal
     */
    private int networkPosition = 1;
    //Socket stuff
    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    //For UI
    private String screen;
    //Main Menu
    private boolean startup = true;
    private final ChannelSettings[] channels = new ChannelSettings[160];
    private final LinearLayout[] layouts = new LinearLayout[40];
    //Message to server
    private boolean[] sending = {false};
    private final boolean[] contStimBoolean = {false};
    private final int[] periodDesiredPosition = {0};
    private final int[] phaseDelayValue = {0};
    private final int[] maxPhaseWidthPosition = {0};
    private final int[] maxPhaseAmpPosition = {0};
    private final int[] triggerOutWidthPosition = {0};
    private final int[] numOfPulsesPosition = {0};
    private final int[] packetSeparation = {0};
    private String[] interphaseArray = {"0"};
    private String[] ampArray = {"0"};
    private String[] widthArray = {"0"};
    //NECSIS
    private final String[] numberOfData = {"0"};
    private final String[] numberOfPacket = {"0"};
    private final String[] settleTime = {"0"};
    private final String[] telemetryStart = {"0"};
    private final String[] tailInclude = {"0"};
    private final String[] packetHeader = {"0"};
    private final String[] leadingZeroLength = {"0"};
    private final String[] numberOfRepetition = {"0"};
    private final String[] gapLength = {"0"};
    private final String[] triggerStart = {"0"};
    private final String[] triggerStop = {"0"};
    private final String[] triggerPermanent = {"0"};
    private final String[] triggerSelect = {"0"};
    private final String[] timeStampDelay = {"0"};
    private final int[] adcEnablePosition = {0};
    private final int[] muxModePosition = {1};
    private final int[] muxStartPosition = {0};
    private final int[] muxHeadDelayPosition = {0};
    private final int[] muxSelectPosition = {0};
    private final String[] afeControl = {"0"};
    private final int[] clockDivPosition = {0};
    private final int[] resetPosition = {0};
    private final String[] simulatorTimeStamp = {"0"};
    private final String[] muxHead = {"0"};
    private final String[] adcClock = {"0"};
    private final String[] triggerClock = {"0"};
    private final String[] muxClock = {"0"};
    //Switches
    private final boolean[] pin58boolean = {false};
    private final boolean[] pin59boolean = {false};
    private final boolean[] pin60boolean = {false};
    private final boolean[] pin61boolean = {false};
    private final boolean[] pin62boolean = {false};
    private final boolean[] pin63boolean = {false};
    private final boolean[] pin64boolean = {false};
    private final boolean[] pin03boolean = {false};
    private final boolean[] pin15boolean = {false};
    private final boolean[] pin16boolean = {false};
    private final boolean[] pin17boolean = {false};
    private final boolean[] pin18boolean = {false};
    private final boolean[] pin04boolean = {false};
    private final boolean[] pin45boolean = {false};
    private final boolean[] pin50boolean = {false};
    private final boolean[] pin53boolean = {false};
    //Recording
    public final static int GRAPH_UPDATE_RATE = 100;
    private ArrayList<MyXYSeries> xySeriesArrayList;
    private ArrayList<GraphicalView> graphs;
    private ArrayList<XYMultipleSeriesRenderer> seriesRenderers;
    //Data Storage
    public int MAX_GRAPHS = 0;
    private final String dataFileName = "graph_data";
    private final String dataFileDirectory = Environment.getExternalStorageDirectory().getPath() + File.separator;
    private final String dataFilePath = dataFileDirectory + dataFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Connect to WiFi in a separate thread and then start the program
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            ProgressDialog progressDialog;
            final boolean[] connected = {false};

            @Override
            protected Void doInBackground(Void... params) {
                connected[0] = ConfigureAndConnectToWifi(networkPosition);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                setMainMenuScreen();
                if(connected[0]) {
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = helpBuilder.create();
                    dialog.setMessage("Unable to establish a WiFi connection");
                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "NETWORK SETTINGS", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            setNetworkScreen();
                        }
                    });
                    dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CONTINUE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }

                //Socket stuff
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                socket = null;
                dataInputStream = null;
                dataOutputStream = null;

                xySeriesArrayList = new ArrayList<>();
                Preset.makeDirectory(dataFileDirectory);
//                Preset.deleteAllPresets(dataFileDirectory);
            }

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(MainActivity.this,
                        "Loading Application", "", true);
                progressDialog.show();

                for (int i = 0; i < layouts.length; i++) {
                    layouts[i] = new LinearLayout(MainActivity.this, null, R.style.button_norm);
                    LinearLayout.LayoutParams layoutLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layouts[i].setLayoutParams(layoutLayoutParams);
                    layouts[i].setOrientation(LinearLayout.HORIZONTAL);
                    layouts[i].setMinimumHeight(80);
                    layouts[i].setMinimumWidth(25);

                    for (int j = 4 * i; j < 4 * i + 4; j++) {
                        channels[j] = new ChannelSettings(MainActivity.this);
                        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                        buttonLayoutParams.weight = 1;
                        buttonLayoutParams.setMargins(4, 4, 4, 4);
                        channels[j].setLayoutParams(buttonLayoutParams);
                        String s = "" + (j + 1);
                        channels[j].setText(s);
                        channels[j].setID(j + 1);
                        channels[j].setTextSize(25);
                        channels[j].setTextColor(Color.BLACK);
                        layouts[i].addView(channels[j]);
                    }
                }
            }
        };
        task.execute();
    }

    private void setMainMenuScreen() {
        setContentView(R.layout.main_screen);
        screen = "main_menu";

        //Initialize Views
        Button presetsButton = (Button) findViewById(R.id.presetsButton);
        Button saveButton = (Button) findViewById(R.id.saveButton);
        Button setSelectedChannels = (Button) findViewById(R.id.setSelectedChannelsButton);
        Button globalSettings = (Button) findViewById(R.id.globalSettings);
        Button setNecsis = (Button) findViewById(R.id.setNecsis);
        Button recording = (Button) findViewById(R.id.recordingButton);
        Button network = (Button) findViewById(R.id.networkButton);
        Button clearChannels = (Button) findViewById(R.id.clearChannelsButton);
        final Button deselectChannels = (Button) findViewById(R.id.deselectChannelsButton);
        final Button startstop = (Button) findViewById(R.id.start_stop);
        final Button showWaveforms = (Button) findViewById(R.id.showWaveforms);
        final Button setSwitches = (Button) findViewById(R.id.setSwitches);
        final Button customMessage = (Button) findViewById(R.id.customMessageButton);
        final CheckBox receiving = (CheckBox) findViewById(R.id.receivingCheckBox);

        updateArrays();

        //Create channels and create layout
        LinearLayout channelLayout = (LinearLayout) findViewById(R.id.scrollLinearLayout);
        for (LinearLayout layout : layouts) {
            if (!startup) {
                ((ViewGroup) layout.getParent()).removeView(layout);
            }
            channelLayout.addView(layout);
        }
        startup = false;

        //Channel Buttons
        for (final ChannelSettings channel : channels){
            channel.setBackground(channel.isSet() ? getDrawable(R.drawable.button_set) : getDrawable(R.drawable.button_norm));
            channel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    channel.setSelected(!channel.isSelected());
                    if (channel.isSet()) {
                        channel.setBackground(!channel.isSelected() ? getDrawable(R.drawable.button_set) : getDrawable(R.drawable.button_set_select));
                    } else {
                        channel.setBackground(!channel.isSelected() ? getDrawable(R.drawable.button_norm) : getDrawable(R.drawable.button_norm_select));
                    }
                }
            });
        }

        //Clear All
        clearChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectAllChannels();
                for (final ChannelSettings channel : channels) {
                    channel.clear();
                    channel.setBackground(getDrawable(R.drawable.button_norm));
                }
            }
        });

        //Deselect All
        deselectChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectAllChannels();
                for (ChannelSettings channel : channels) {
                    channel.setBackground(channel.isSet() ? getDrawable(R.drawable.button_set) :
                            getDrawable(R.drawable.button_norm));
                }
            }
        });

        //Presets
        presetsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPresetsListScreen();
            }
        });

        //Save
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSavePresetScreen();
            }
        });

        //Set Channels
        setSelectedChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Integer> selectedChannels = new ArrayList<>();
                for (ChannelSettings channel : channels) {
                    if (channel.isSelected())
                        selectedChannels.add(channel.getChannelID() - 1);
                }
                if (selectedChannels.isEmpty()) {
                    final AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = helpBuilder.create();
                    dialog.setMessage("Please select at least one channel.");
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    setChannelsScreen(selectedChannels);
                }
            }
        });

        //Global Settings
        globalSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGlobalSettingsScreen();
            }
        });

        //Set NECSIS
        setNecsis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNecsisScreen();
            }
        });

        //Custom Message
        customMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCustomMessageScreen();
            }
        });

        //Start/Stop Recording
        //  Draw the start/stop button correctly depending on whether information is being sent
        //  or not
        startstop.setTextSize(14);
        startstop.setText(!sending[0] ? "START PULSE TRAIN" : "STOP PULSE TRAIN");
        startstop.setBackground(!sending[0] ? getDrawable(R.drawable.button_set) : getDrawable(R.drawable.button_set_select));
        startstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sending[0]) {
                    sendStop();
                    sending[0] = false;
                    startstop.setText(R.string.start);
                    startstop.setBackground(getDrawable(R.drawable.button_set));
                } else {
                    final AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = helpBuilder.create();
                    // Count the number of channels selected
                    boolean setNoChannels = muxModePosition[0] == 0;
                    // Check if number of graphs selected is valid
                    if (setNoChannels) {
                        dialog.setMessage("Please select MUX Mode channels.");
                        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } else {
                        // Check if there is already a recording set up
                        if (!xySeriesArrayList.isEmpty()) {
                            dialog.setMessage("Do you want to start a new recording?");
                            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    sending[0] = true;
                                    startstop.setText(R.string.stop);
                                    startstop.setBackground(getDrawable(R.drawable.button_set_select));
                                    xySeriesArrayList = new ArrayList<>();
                                    resetTextDataFile();
                                    setUpGraphs();
                                    sendMessage();
                                }
                            });
                            dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            dialog.setMessage("Do you want to start a new recording?");
                            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    sending[0] = true;
                                    startstop.setText(R.string.stop);
                                    startstop.setBackground(getDrawable(R.drawable.button_set_select));
                                    xySeriesArrayList = new ArrayList<>();
                                    resetTextDataFile();
                                    setUpGraphs();
                                    sendMessage();
                                }
                            });
                            dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }
                    }
                }
            }
        });

        //Recording
        recording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecordingScreen();
            }
        });

        //Receiving Data Checkbox
        receiving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expectingReceivingData[0] = !expectingReceivingData[0];
            }
        });
        receiving.setChecked(expectingReceivingData[0]);

        //Network
        network.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNetworkScreen();
            }
        });

        //Waveform
        showWaveforms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWaveform();
            }
        });

        //Switches
        setSwitches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSwitchesScreen();
            }
        });

    }

    private void setRecordingScreen() {
        setContentView(R.layout.recording);
        screen = "recording";

        // Set up the email-data button
        Button send = (Button) findViewById(R.id.send_recording);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Enter Recipient Email Address");

                final EditText input = new EditText(MainActivity.this);
                input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                builder.setView(input);

                builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String email = input.getText().toString();

                        AsyncTask<Void, Void, Void> emailTask = new AsyncTask<Void, Void, Void>() {
                            ProgressDialog progressDialog;
                            final boolean[] connected = {false};

                            @Override
                            protected Void doInBackground(Void... params) {
                                connected[0] = sendDataPoints(email);
                                return null;
                            }

                            @Override
                            protected void onPreExecute() {
                                progressDialog = ProgressDialog.show(MainActivity.this, "Sending Data", "", true);
                                progressDialog.show();
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                if(connected[0]) {
                                    progressDialog.dismiss();
                                } else {
                                    AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                                    AlertDialog dialog = helpBuilder.create();
                                    dialog.setMessage("Unable to establish a WiFi connection");
                                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "TO NETWORK SETTINGS", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            setNetworkScreen();
                                        }
                                    });
                                    dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "IGNORE", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                }
                            }

                        };
                        emailTask.execute();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        // Create graph renderers based on the XYSeries and adds GraphicalViews to
        // the recording screen
        for (int i = 0; i < xySeriesArrayList.size(); i++) {
            XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
            dataset.addSeries(xySeriesArrayList.get(i));

            // Add graph to layout and graphs array
            LinearLayout layout = (LinearLayout) findViewById(R.id.hi);
            GraphicalView graphicalView = ChartFactory.getLineChartView(this, dataset, seriesRenderers.get(i));
            graphicalView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    600));
            graphicalView.setBackgroundColor(Color.WHITE);
            // Prevent focusing which leads to crashing of graph
            graphicalView.setFocusable(false);
            graphs.add(graphicalView);
            layout.addView(graphicalView);
        }

    }

    private void setGlobalSettingsScreen() {
        screen = "global_settings";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.global_setting, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Initialize things
        CheckBox contStim = (CheckBox) dialoglayout.findViewById(R.id.contStimChkBox);
        Spinner periodDesired = (Spinner) dialoglayout.findViewById(R.id.periodDesiredSpinner);
        Spinner phaseDelay = (Spinner) dialoglayout.findViewById(R.id.phaseDelaySpinner);
        Spinner maxPhaseWidth = (Spinner) dialoglayout.findViewById(R.id.maxPhaseWidthSpinner);
        Spinner maxPhaseAmp = (Spinner) dialoglayout.findViewById(R.id.maxPhaseAmpSpinner);
        Spinner triggerOutWidth = (Spinner) dialoglayout.findViewById(R.id.triggerOutWidthSpinner);
        Spinner numOfPulses = (Spinner) dialoglayout.findViewById(R.id.numOfPulsesSpinner);
        EditText packetSeparationText = (EditText) dialoglayout.findViewById(R.id.packetSeparationEditText);

        //Temp Variables
        final boolean[] contStimBoolean = this.contStimBoolean;
        final int[] periodDesiredPosition = this.periodDesiredPosition;
        final int[] phaseDelayValue = this.phaseDelayValue;
        final int[] maxPhaseWidthPosition = this.maxPhaseWidthPosition;
        final int[] maxPhaseAmpPosition = this.maxPhaseAmpPosition;
        final int[] triggerOutWidthPosition = this.triggerOutWidthPosition;
        final int[] numOfPulsesPosition = this.numOfPulsesPosition;
        final int[] packetSeparation = this.packetSeparation;

        //Period Desire (ms)
        ArrayAdapter<CharSequence> periodDesiredAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.periodDesiredArray, R.layout.global_settings_spinner_item);
        periodDesiredAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        periodDesired.setAdapter(periodDesiredAdapter);
        periodDesired.setSelection(periodDesiredPosition[0]);
        periodDesired.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            periodDesiredPosition[0] = position;
        }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Phase Delay
        ArrayAdapter<CharSequence> phaseDelayAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.phaseDelayArray, R.layout.global_settings_spinner_item);
        phaseDelayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        phaseDelay.setAdapter(phaseDelayAdapter);
        phaseDelay.setSelection(phaseDelayValue[0]);
        phaseDelay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phaseDelayValue[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Max Phase Width (ms)
        ArrayAdapter<CharSequence> maxPhaseWidthAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.maxPhaseWidthArray, R.layout.global_settings_spinner_item);
        maxPhaseWidthAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        maxPhaseWidth.setAdapter(maxPhaseWidthAdapter);
        maxPhaseWidth.setSelection(maxPhaseWidthPosition[0]);
        maxPhaseWidth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maxPhaseWidthPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Max Phase Amp (uA)
        ArrayAdapter<CharSequence> maxPhaseAmpAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.maxPhaseAmpArray, R.layout.global_settings_spinner_item);
        maxPhaseAmpAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        maxPhaseAmp.setAdapter(maxPhaseAmpAdapter);
        maxPhaseAmp.setSelection(maxPhaseAmpPosition[0]);
        maxPhaseAmp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maxPhaseAmpPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Trigger Out Width (uS)
        ArrayAdapter<CharSequence> triggerOutWidthAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.triggerOutWidthArray, R.layout.global_settings_spinner_item);
        triggerOutWidthAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        triggerOutWidth.setAdapter(triggerOutWidthAdapter);
        triggerOutWidth.setSelection(triggerOutWidthPosition[0]);
        triggerOutWidth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                triggerOutWidthPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Number of Pulses per Train
        ArrayAdapter<CharSequence> numOfPulsesAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.numOfPulsesArray, R.layout.global_settings_spinner_item);
        numOfPulsesAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        numOfPulses.setAdapter(numOfPulsesAdapter);
        numOfPulses.setSelection(numOfPulsesPosition[0]);
        numOfPulses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                numOfPulsesPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Packet Separation
        packetSeparationText.setText("" + packetSeparation[0]);

        //Continuous Stimulation
        contStim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contStimBoolean[0] = !contStimBoolean[0];
            }
        });
        if (contStimBoolean[0]) contStim.setChecked(true);

        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.contStimBoolean[0] = contStimBoolean[0];
                MainActivity.this.periodDesiredPosition[0] = periodDesiredPosition[0];
                MainActivity.this.phaseDelayValue[0] = phaseDelayValue[0];
                MainActivity.this.maxPhaseWidthPosition[0] = maxPhaseWidthPosition[0];
                MainActivity.this.maxPhaseAmpPosition[0] = maxPhaseAmpPosition[0];
                MainActivity.this.triggerOutWidthPosition[0] = triggerOutWidthPosition[0];
                MainActivity.this.numOfPulsesPosition[0] = numOfPulsesPosition[0];
                MainActivity.this.packetSeparation[0] = packetSeparation[0];
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setChannelsScreen(final ArrayList<Integer> selectedChannels) {
        screen = "set_channels";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.set_selected_channels, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Initialize things
        Button aAmpMinus = (Button) dialoglayout.findViewById(R.id.aAmpMinusButton);
        final Spinner aAmpSpinner = (Spinner) dialoglayout.findViewById(R.id.aAmpSpinner);
        Button aAmpPlus = (Button) dialoglayout.findViewById(R.id.aAmpPlusButton);
        Button cAmpMinus = (Button) dialoglayout.findViewById(R.id.cAmpMinusButton);
        final Spinner cAmpSpinner = (Spinner) dialoglayout.findViewById(R.id.cAmpSpinner);
        Button cAmpPlus = (Button) dialoglayout.findViewById(R.id.cAmpPlusButton);
        Button aPhaseWidthMinus = (Button) dialoglayout.findViewById(R.id.aPhaseWidthMinusButton);
        final Spinner aPhaseWidthSpinner = (Spinner) dialoglayout.findViewById(R.id.aPhaseWidthSpinner);
        Button aPhaseWidthPlus = (Button) dialoglayout.findViewById(R.id.aPhaseWidthPlusButton);
        Button cPhaseWidthMinus = (Button) dialoglayout.findViewById(R.id.cPhaseWidthMinusButton);
        final Spinner cPhaseWidthSpinner = (Spinner) dialoglayout.findViewById(R.id.cPhaseWidthSpinner);
        Button cPhaseWidthPlus = (Button) dialoglayout.findViewById(R.id.cPhaseWidthPlusButton);
        Button startDelayMinus = (Button) dialoglayout.findViewById(R.id.startDelayMinusButton);
        final Spinner startDelaySpinner = (Spinner) dialoglayout.findViewById(R.id.startDelaySpinner);
        Button startDelayPlus = (Button) dialoglayout.findViewById(R.id.startDelayPlusButton);
        Button interPhaseDelayMinus = (Button) dialoglayout.findViewById(R.id.interPhaseDelayMinusButton);
        final Spinner interPhaseDelaySpinner = (Spinner) dialoglayout.findViewById(R.id.interPhaseDelaySpinner);
        Button interPhaseDelayPlus = (Button) dialoglayout.findViewById(R.id.interPhaseDelayPlusButton);
        final CheckBox reversePolarity = (CheckBox) dialoglayout.findViewById(R.id.reversePolarityChkBox);
        final CheckBox chargeCancellation = (CheckBox) dialoglayout.findViewById(R.id.chargeCancelChkBox);

        // Temporary positions
        final int[] aAmpPosition = {0};
        final int[] cAmpPosition = {0};
        final int[] aPhaseWidthPosition = {0};
        final int[] cPhaseWidthPosition = {0};
        final int[] startDelayPosition = {0};
        final int[] interPhaseDelayPosition = {0};
        final boolean[] reversePolarityBoolean = {false};
        final boolean[] chargeCancellationBoolean = {false};

        // Create local amp and width arrays
        updateArrays();

        //Anodic Amp
        ArrayAdapter<CharSequence> aAmpAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.set_channels_spinner_item, ampArray);
        aAmpAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        aAmpSpinner.setAdapter(aAmpAdapter);
        aAmpSpinner.setSelection(aAmpPosition[0]);
        aAmpSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                aAmpPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        aAmpMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aAmpPosition[0] > 0) aAmpPosition[0] -= 1;
                aAmpSpinner.setSelection(aAmpPosition[0]);
            }
        });
        aAmpPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aAmpPosition[0] < 15) aAmpPosition[0] += 1;
                aAmpSpinner.setSelection(aAmpPosition[0]);
            }
        });

        //Cathodic Amp
        ArrayAdapter<CharSequence> cAmpAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.set_channels_spinner_item, ampArray);
        cAmpAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        cAmpSpinner.setAdapter(cAmpAdapter);
        cAmpSpinner.setSelection(cAmpPosition[0]);
        cAmpSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cAmpPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cAmpMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cAmpPosition[0] > 0) cAmpPosition[0] -= 1;
                cAmpSpinner.setSelection(cAmpPosition[0]);
            }
        });
        cAmpPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cAmpPosition[0] < 15) cAmpPosition[0] += 1;
                cAmpSpinner.setSelection(cAmpPosition[0]);
            }
        });

        //Anodic Phase Width
        ArrayAdapter<CharSequence> aPhaseWidthAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.set_channels_spinner_item, widthArray);
        aPhaseWidthAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        aPhaseWidthSpinner.setAdapter(aPhaseWidthAdapter);
        aPhaseWidthSpinner.setSelection(aPhaseWidthPosition[0]);
        aPhaseWidthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                aPhaseWidthPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        aPhaseWidthMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aPhaseWidthPosition[0] > 0) aPhaseWidthPosition[0] -= 1;
                aPhaseWidthSpinner.setSelection(aPhaseWidthPosition[0]);
            }
        });
        aPhaseWidthPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aPhaseWidthPosition[0] < 7) aPhaseWidthPosition[0] += 1;
                aPhaseWidthSpinner.setSelection(aPhaseWidthPosition[0]);
            }
        });

        //Cathodic Phase Width
        ArrayAdapter<CharSequence> cPhaseWidthAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.set_channels_spinner_item, widthArray);
        cPhaseWidthAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        cPhaseWidthSpinner.setAdapter(cPhaseWidthAdapter);
        cPhaseWidthSpinner.setSelection(cPhaseWidthPosition[0]);
        cPhaseWidthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cPhaseWidthPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cPhaseWidthMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cPhaseWidthPosition[0] > 0) cPhaseWidthPosition[0] -= 1;
                cPhaseWidthSpinner.setSelection(cPhaseWidthPosition[0]);
            }
        });
        cPhaseWidthPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cPhaseWidthPosition[0] < 7) cPhaseWidthPosition[0] += 1;
                cPhaseWidthSpinner.setSelection(cPhaseWidthPosition[0]);
            }
        });

        //Inter Phase Delay
        ArrayAdapter<CharSequence> interPhaseDelayAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.set_channels_spinner_item, interphaseArray);
        interPhaseDelayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        interPhaseDelaySpinner.setAdapter(interPhaseDelayAdapter);
        interPhaseDelaySpinner.setSelection(interPhaseDelayPosition[0]);
        interPhaseDelaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                interPhaseDelayPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        interPhaseDelayMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (interPhaseDelayPosition[0] > 0) interPhaseDelayPosition[0] -= 1;
                interPhaseDelaySpinner.setSelection(interPhaseDelayPosition[0]);
            }
        });
        interPhaseDelayPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (interPhaseDelayPosition[0] < 7) interPhaseDelayPosition[0] += 1;
                interPhaseDelaySpinner.setSelection(interPhaseDelayPosition[0]);
            }
        });

        //Start Delay
        ArrayAdapter<CharSequence> startDelayAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.startDelayArray, R.layout.set_channels_spinner_item);
        startDelayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        startDelaySpinner.setAdapter(startDelayAdapter);
        startDelaySpinner.setSelection(startDelayPosition[0]);
        startDelaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                startDelayPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        startDelayMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startDelayPosition[0] > 0) startDelayPosition[0] -= 1;
                startDelaySpinner.setSelection(startDelayPosition[0]);
            }
        });
        startDelayPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startDelayPosition[0] < 50) startDelayPosition[0] += 1;
                startDelaySpinner.setSelection(startDelayPosition[0]);
            }
        });

        //Reverse Polarity
        reversePolarity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reversePolarityBoolean[0] = !reversePolarityBoolean[0];
                reversePolarity.setChecked(reversePolarityBoolean[0]);
            }
        });

        //Charge Cancellation
        chargeCancellation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chargeCancellationBoolean[0] = !chargeCancellationBoolean[0];
                chargeCancellation.setChecked(chargeCancellationBoolean[0]);
            }
        });

        //Save and Clear and Cancel Buttons
        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (Integer i : selectedChannels) {
                    ChannelSettings channel = channels[i];
                    channel.set(aAmpPosition[0], cAmpPosition[0], aPhaseWidthPosition[0],
                            cPhaseWidthPosition[0], startDelayPosition[0], interPhaseDelayPosition[0],
                            reversePolarityBoolean[0], chargeCancellationBoolean[0]);
                }
                final AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog dialog1 = helpBuilder.create();
                dialog1.setMessage("Saved.");
                dialog1.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        deselectAllChannels();
                        setMainMenuScreen();
                    }
                });
                dialog1.show();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (Integer i : selectedChannels) {
                    ChannelSettings channel = channels[i];
                    channel.setSelected(false);
                }
                deselectAllChannels();
                setMainMenuScreen();
                final AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog dialog1 = helpBuilder.create();
                dialog1.setMessage("Cleared.");
                dialog1.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog1.show();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setPresetsListScreen() {
        screen = "presets_list";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.presets_scroll_list, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();

        ArrayList<Preset> list = Preset.getPresets(dataFileDirectory);

        LinearLayout layout = (LinearLayout) dialoglayout.findViewById(R.id.presetListLayout);
        for (final Preset preset : list) {
            final Button button = new Button(MainActivity.this);
            button.setText(preset.name);
            button.setGravity(Gravity.CENTER);
            button.setBackground(getDrawable(R.drawable.button_norm));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    ((ViewGroup) button.getParent()).removeAllViews();
                    setPresetLoadScreen(preset);
                }
            });
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 10, 10);
            button.setLayoutParams(params);
            layout.addView(button);
//            Log.v("Presets", "1");
        }

        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setPresetLoadScreen(final Preset preset) {
        screen = "preset_load";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.preset_load, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        EditText title = (EditText) dialoglayout.findViewById(R.id.title1);
        title.setEnabled(false);
        title.setShowSoftInputOnFocus(false);
        title.clearFocus();
        title.setText(preset.name);

        TextView body = (TextView) dialoglayout.findViewById(R.id.body);
        String bodyText = "";

        if (preset.localSettings.isSet()) {
            int periodDesiredValue = Integer.parseInt(getResources().getStringArray(
                    R.array.periodDesiredArray)[preset.periodDesiredPosition]);
            double maxPhaseWidthValue = Double.parseDouble(getResources().getStringArray(
                    R.array.maxPhaseWidthArray)[preset.maxPhaseWidthPosition]);
            int maxPhaseAmpValue = Integer.parseInt(getResources().getStringArray(
                    R.array.maxPhaseAmpArray)[preset.maxPhaseAmpPosition]);
            int triggerOutWidthValue = Integer.parseInt(getResources().getStringArray(
                    R.array.triggerOutWidthArray)[preset.triggerOutWidthPosition]);
            int numOfPulsesValue = Integer.parseInt(getResources().getStringArray(
                    R.array.numOfPulsesArray)[preset.numOfPulsesPosition]);

            String[] tempAmpArray = new String[16];
            for (int i = 0; i < tempAmpArray.length; i++) {
                tempAmpArray[i] = "" + (Math.floor(i * maxPhaseAmpValue / (tempAmpArray.length - 1) * 100) / 100);
            }
            String[] tempWidthArray = new String[8];
            for (int i = 0; i < tempWidthArray.length; i++) {
                tempWidthArray[i] = "" + (Math.floor(i * maxPhaseWidthValue / (tempWidthArray.length - 1) * 10000) / 10000);
            }
            String[] tempInterphaseArray = new String[8];
            for (int i = 0; i < tempInterphaseArray.length; i++) {
                tempInterphaseArray[i] = "" + (i * maxPhaseWidthValue / tempInterphaseArray.length);
            }

            bodyText += "Period Desired: " + periodDesiredValue + "\n";
            bodyText += "Phase Shift: " + preset.phaseShiftPosition + "\n";
            bodyText += "Max Phase Width: " + maxPhaseWidthValue + "\n";
            bodyText += "Max Phase Amp: " + maxPhaseAmpValue + "\n";
            bodyText += "Trigger Out Width: " + triggerOutWidthValue + "\n";
            bodyText += "Number of Pulses: " + numOfPulsesValue + "\n";
            bodyText += "Continuous Stimulation: " + preset.contStimBoolean + "\n\n";
            for (Preset.Settings setting : preset.localSettings.getList()) {
                double aAmpValue = Double.parseDouble(tempAmpArray[setting.aAmpPosition]);
                double cAmpValue = Double.parseDouble(tempAmpArray[setting.cAmpPosition]);
                double aPhaseWidthValue = Double.parseDouble(tempWidthArray[setting.aPhaseWidthPosition]);
                double cPhaseWidthValue = Double.parseDouble(tempWidthArray[setting.cPhaseWidthPosition]);
                double interPhaseDelayValue = Double.parseDouble(tempInterphaseArray[setting.interPhaseDelayPosition]);
                double startDelayValue = Double.parseDouble(getResources().getStringArray(
                        R.array.startDelayArray)[setting.startDelayPosition]);

                bodyText += "Channel: " + setting.channelNumber + "\n";
                bodyText += "Anodic Amp: " + aAmpValue + "\n";
                bodyText += "Cathodic Amp: " + cAmpValue + "\n";
                bodyText += "Anodic Phase Width: " + aPhaseWidthValue + "\n";
                bodyText += "Cathodic Phase Width: " + cPhaseWidthValue + "\n";
                bodyText += "Inter Phase Delay: " + interPhaseDelayValue + "\n";
                bodyText += "Start Delay: " + startDelayValue + "\n";
                bodyText += "Reverse Polarity: " + setting.reversePolarity + "\n";
                bodyText += "Charge Cancellation: " + setting.chargeCancellation + "\n\n";
            }
        }
        body.setText(bodyText);

        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Load", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                setSettingsFromPreset(preset);
                setMainMenuScreen();
                alertDialog.dismiss();
                AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog dialog = helpBuilder.create();
                dialog.setMessage("Preset loaded");
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog dialog = helpBuilder.create();
                dialog.setMessage("Are you sure you want to delete this preset?");
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Preset.deletePreset(dataFileDirectory, preset);
                        dialog.dismiss();
                        alertDialog.dismiss();
                        setPresetsListScreen();
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        final AlertDialog dialog2 = builder.create();
                        dialog2.setMessage("Preset deleted");
                        dialog2.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog2.dismiss();
                            }
                        });
                        dialog2.show();
                    }                });
                dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                setPresetsListScreen();
            }
        });
        alertDialog.show();
    }

    private void setSavePresetScreen() {
        screen = "preset_save";
        final LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.preset_save, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        LinearLayout layout = (LinearLayout) dialoglayout.findViewById(R.id.presetLayout);
        layout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    hideKeyboard();
            }
        });

        TextView body = (TextView) dialoglayout.findViewById(R.id.body);
        body.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard();
                    Log.v("Keyboard", "Body");
                }
            }
        });

        final EditText title = (EditText) dialoglayout.findViewById(R.id.title);

        String bodyText = "";

        int periodDesiredValue = Integer.parseInt(getResources().getStringArray(
                R.array.periodDesiredArray)[periodDesiredPosition[0]]);
        double maxPhaseWidthValue = Double.parseDouble(getResources().getStringArray(
                R.array.maxPhaseWidthArray)[maxPhaseWidthPosition[0]]);
        int maxPhaseAmpValue = Integer.parseInt(getResources().getStringArray(
                R.array.maxPhaseAmpArray)[maxPhaseAmpPosition[0]]);
        int triggerOutWidthValue = Integer.parseInt(getResources().getStringArray(
                R.array.triggerOutWidthArray)[triggerOutWidthPosition[0]]);
        int numOfPulsesValue = Integer.parseInt(getResources().getStringArray(
                R.array.numOfPulsesArray)[numOfPulsesPosition[0]]);

        bodyText += "Period Desired: " + periodDesiredValue + "\n";
        bodyText += "Phase Shift: " + phaseDelayValue[0] + "\n";
        bodyText += "Max Phase Width: " + maxPhaseWidthValue + "\n";
        bodyText += "Max Phase Amp: " + maxPhaseAmpValue + "\n";
        bodyText += "Trigger Out Width: " + triggerOutWidthValue + "\n";
        bodyText += "Number of Pulses: " + numOfPulsesValue + "\n";
        bodyText += "Continuous Stimulation: " + contStimBoolean[0] + "\n\n";
        for (ChannelSettings channel : channels) {
            if (channel.isSet()) {
                double aAmpValue = Double.parseDouble(ampArray[channel.getaAmpPosition()]);
                double cAmpValue = Double.parseDouble(ampArray[channel.getcAmpPosition()]);
                double aPhaseWidthValue = Double.parseDouble(widthArray[channel.getaPhaseWidthPosition()]);
                double cPhaseWidthValue = Double.parseDouble(widthArray[channel.getcPhaseWidthPosition()]);
                double startDelayValue = Double.parseDouble(getResources().getStringArray(
                        R.array.startDelayArray)[channel.getStartDelayPosition()]);
                double interPhaseDelayValue = Double.parseDouble(interphaseArray[channel.getInterPhaseDelayPosition()]);
                bodyText += "Channel: " + channel.getChannelID() + "\n";
                bodyText += "Anodic Amp: " + aAmpValue + "\n";
                bodyText += "Cathodic Amp: " + cAmpValue + "\n";
                bodyText += "Anodic Phase Width: " + aPhaseWidthValue + "\n";
                bodyText += "Cathodic Phase Width: " + cPhaseWidthValue + "\n";
                bodyText += "Inter Phase Delay: " + interPhaseDelayValue + "\n";
                bodyText += "Start Delay: " + startDelayValue + "\n";
                bodyText += "Reverse Polarity: " + channel.isReversePolarity() + "\n";
                bodyText += "Charge Cancellation: " + channel.isChargeCancellation() + "\n\n";
            }
        }
        body.setText(bodyText);

        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                if(title.getText().toString().trim().equals("")) {
                    AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = helpBuilder.create();
                    dialog.setMessage("The name cannot be empty");
                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "RETRY",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    setSavePresetScreen();
                                }
                            });
                    dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "BACK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    dialog.show();
                } else {
                    hideKeyboard();
                    Preset current = new Preset();
                    current.name = title.getText().toString().toUpperCase();
                    current.contStimBoolean = contStimBoolean[0];
                    current.periodDesiredPosition = periodDesiredPosition[0];
                    current.maxPhaseWidthPosition = maxPhaseWidthPosition[0];
                    current.maxPhaseAmpPosition = maxPhaseAmpPosition[0];
                    current.triggerOutWidthPosition = triggerOutWidthPosition[0];
                    current.numOfPulsesPosition = numOfPulsesPosition[0];
                    current.phaseShiftPosition = phaseDelayValue[0];
                    for (ChannelSettings channel : channels) {
                        if (channel.isSet()) {
                            current.addLocalSetting(channel);
                        }
                    }
                    String result = current.saveToFile(dataFileDirectory);
                    Log.v("Preset", result);
                    switch (result) {
                        case Preset.FILE_EXISTS: {
                            AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                            AlertDialog dialog = helpBuilder.create();
                            dialog.setMessage("The File Name Already Exists!");
                            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            dialog.show();
                            break;
                        }
                        case Preset.SUCCESS: {
                            AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                            AlertDialog dialog = helpBuilder.create();
                            dialog.setMessage("Saved!");
                            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            dialog.show();
                            break;
                        }
                        case Preset.ERROR: {
                            AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);
                            AlertDialog dialog = helpBuilder.create();
                            dialog.setMessage("Error! Please Try Again.");
                            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            dialog.show();
                            break;
                        }
                    }
                }
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setSettingsFromPreset(final Preset preset) {
        periodDesiredPosition[0] = preset.periodDesiredPosition;
        maxPhaseWidthPosition[0] = preset.maxPhaseWidthPosition;
        maxPhaseAmpPosition[0] = preset.maxPhaseAmpPosition;
        triggerOutWidthPosition[0] = preset.triggerOutWidthPosition;
        numOfPulsesPosition[0] = preset.numOfPulsesPosition;
        contStimBoolean[0] = preset.contStimBoolean;
        phaseDelayValue[0] = preset.phaseShiftPosition;

        for (Preset.Settings setting : preset.localSettings.getList()) {
            int channelIndex = setting.channelNumber - 1;
            channels[channelIndex].set(setting);
        }
        updateArrays();
    }

    private void setNetworkScreen() {
        screen = "network";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        final AlertDialog[] alertDialog = {null};

        if (networkPosition == 0) {
            dialoglayout = inflater.inflate(R.layout.network_settings_enterprise, null);

            LinearLayout layout = (LinearLayout) dialoglayout.findViewById(R.id.networkLayout);
            layout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        hideKeyboard();
                }
            });

            Spinner securitySpinner = (Spinner) dialoglayout.findViewById(R.id.securitySpinner);
            ArrayAdapter<CharSequence> networkSecurityAdapter = ArrayAdapter.createFromResource(
                    getApplicationContext(), R.array.networkSecuritiesArray, R.layout.custom_spinner_dropdown_item);
            networkSecurityAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
            securitySpinner.setAdapter(networkSecurityAdapter);
            securitySpinner.setSelection(0);
            securitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    hideKeyboard();
                    networkPosition = position;
                    if (networkPosition != 0) {
                        alertDialog[0].dismiss();
                        setNetworkScreen();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    hideKeyboard();
                }
            });

            final EditText ssid = (EditText) dialoglayout.findViewById(R.id.SSID);
            final EditText username = (EditText) dialoglayout.findViewById(R.id.username);
            final EditText password = (EditText) dialoglayout.findViewById(R.id.password);
            Button connect = (Button) dialoglayout.findViewById(R.id.connect);

            ssid.setText(networkSSID);
            username.setText(networkEnterpriseUsername);
            password.setText(networkEnterprisePassword);

            connect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    networkSSID = ssid.getText().toString();
                    networkEnterpriseUsername = username.getText().toString();
                    networkEnterprisePassword = password.getText().toString();
                    connectToWifiAlertDialog();
                }
            });

        } else {
            dialoglayout = inflater.inflate(R.layout.network_settings_personal, null);

            LinearLayout layout = (LinearLayout) dialoglayout.findViewById(R.id.networkLayout);
            layout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        hideKeyboard();
                }
            });

            Spinner securitySpinner = (Spinner) dialoglayout.findViewById(R.id.securitySpinner);
            ArrayAdapter<CharSequence> networkSecurityAdapter = ArrayAdapter.createFromResource(
                    getApplicationContext(), R.array.networkSecuritiesArray, R.layout.custom_spinner_dropdown_item);
            networkSecurityAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
            securitySpinner.setAdapter(networkSecurityAdapter);
            securitySpinner.setSelection(1);
            securitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    hideKeyboard();
                    networkPosition = position;
                    if (networkPosition != 1) {
                        alertDialog[0].dismiss();
                        setNetworkScreen();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    hideKeyboard();
                }
            });

            final EditText ssid = (EditText) dialoglayout.findViewById(R.id.SSID);
            final EditText password = (EditText) dialoglayout.findViewById(R.id.password);
            Button connect = (Button) dialoglayout.findViewById(R.id.connect);

            ssid.setText(networkSSID);
            password.setText(networkPersonalPassword);

            connect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    networkSSID = ssid.getText().toString();
                    networkPersonalPassword = password.getText().toString();
                    connectToWifiAlertDialog();
                }
            });
        }

        final EditText ipText = (EditText) dialoglayout.findViewById(R.id.ipEdittext);
        final EditText portText = (EditText) dialoglayout.findViewById(R.id.portEdittext);
        final Button save = (Button) dialoglayout.findViewById(R.id.save);

        ipText.setText(ipAddress);
        String temp = "" + portNumber;
        portText.setText(temp);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ipText.getText().toString().equals("") && !portText.getText().toString().equals("")) {
                    hideKeyboard();
                    ipAddress = ipText.getText().toString();
                    portNumber = Integer.parseInt(portText.getText().toString());
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = builder.create();
                    dialog.setMessage("Saved!");
                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = builder.create();
                    dialog.setMessage("Please enter an IP Address and Port Number.");
                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        });

        builder.setView(dialoglayout);
        alertDialog[0] = builder.create();
        alertDialog[0].setCanceledOnTouchOutside(false);
        alertDialog[0].setButton(DialogInterface.BUTTON_NEUTRAL, "Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog[0].dismiss();
            }
        });
        alertDialog[0].show();
    }

    private void setSwitchesScreen() {
        screen = "switches";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.controller_switches, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Creat Switch objects
        final Switch pin58 = (Switch) dialoglayout.findViewById(R.id.pin58);
        pin58.setChecked(pin58boolean[0]);
        final Switch pin59 = (Switch) dialoglayout.findViewById(R.id.pin59);
        pin59.setChecked(pin59boolean[0]);
        final Switch pin60 = (Switch) dialoglayout.findViewById(R.id.pin60);
        pin60.setChecked(pin60boolean[0]);
        final Switch pin61 = (Switch) dialoglayout.findViewById(R.id.pin61);
        pin61.setChecked(pin61boolean[0]);
        final Switch pin62 = (Switch) dialoglayout.findViewById(R.id.pin62);
        pin62.setChecked(pin62boolean[0]);
        final Switch pin63 = (Switch) dialoglayout.findViewById(R.id.pin63);
        pin63.setChecked(pin63boolean[0]);
        final Switch pin64 = (Switch) dialoglayout.findViewById(R.id.pin64);
        pin64.setChecked(pin64boolean[0]);
        final Switch pin03 = (Switch) dialoglayout.findViewById(R.id.pin03);
        pin03.setChecked(pin03boolean[0]);
        final Switch pin15 = (Switch) dialoglayout.findViewById(R.id.pin15);
        pin15.setChecked(pin15boolean[0]);
        final Switch pin16 = (Switch) dialoglayout.findViewById(R.id.pin16);
        pin16.setChecked(pin16boolean[0]);
        final Switch pin17 = (Switch) dialoglayout.findViewById(R.id.pin17);
        pin17.setChecked(pin17boolean[0]);
        final Switch pin18 = (Switch) dialoglayout.findViewById(R.id.pin18);
        pin18.setChecked(pin18boolean[0]);
        final Switch pin04 = (Switch) dialoglayout.findViewById(R.id.pin04);
        pin04.setChecked(pin04boolean[0]);
        final Switch pin45 = (Switch) dialoglayout.findViewById(R.id.pin45);
        pin45.setChecked(pin45boolean[0]);
        final Switch pin50 = (Switch) dialoglayout.findViewById(R.id.pin50);
        pin50.setChecked(pin50boolean[0]);
        final Switch pin53 = (Switch) dialoglayout.findViewById(R.id.pin53);
        pin53.setChecked(pin53boolean[0]);

        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pin60boolean[0] = pin60.isChecked();
                pin61boolean[0] = pin61.isChecked();
                pin62boolean[0] = pin62.isChecked();
                pin63boolean[0] = pin63.isChecked();
                pin64boolean[0] = pin64.isChecked();
                pin03boolean[0] = pin03.isChecked();
                pin15boolean[0] = pin15.isChecked();
                pin16boolean[0] = pin16.isChecked();
                pin17boolean[0] = pin17.isChecked();
                pin18boolean[0] = pin18.isChecked();
                pin04boolean[0] = pin04.isChecked();
                pin45boolean[0] = pin45.isChecked();
                pin50boolean[0] = pin50.isChecked();
                pin53boolean[0] = pin53.isChecked();
                sendSwitches();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Clear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pin60boolean[0] = false;
                pin61boolean[0] = false;
                pin62boolean[0] = false;
                pin63boolean[0] = false;
                pin64boolean[0] = false;
                pin03boolean[0] = false;
                pin15boolean[0] = false;
                pin16boolean[0] = false;
                pin17boolean[0] = false;
                pin18boolean[0] = false;
                pin04boolean[0] = false;
                pin45boolean[0] = false;
                pin50boolean[0] = false;
                pin53boolean[0] = false;
                setSwitchesScreen();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void connectToWifiAlertDialog() {

        final AlertDialog.Builder helpBuilder = new AlertDialog.Builder(MainActivity.this);

        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this,
                "Attempting to Connect", "", true);
        final boolean[] connected = {false};

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                connected[0] = ConfigureAndConnectToWifi(networkPosition);
                return null;
            }

            @Override
            protected void onPreExecute(){
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(Void result){
                progressDialog.dismiss();
                if (connected[0]) {
                    AlertDialog dialog = helpBuilder.create();
                    dialog.setMessage("Successfully Connected");
                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    hideKeyboard();
                } else {
                    AlertDialog dialog = helpBuilder.create();
                    dialog.setMessage("Connection Failed");
                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            connectToWifiAlertDialog();
                        }
                    });
                    dialog.show();
                }
            }
        };
        task.execute();

    }

    private void setNecsisScreen() {
        setContentView(R.layout.necsis);
        screen = "necsis";

        LinearLayout layout = (LinearLayout) findViewById(R.id.necsisLayout);
        layout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    hideKeyboard();
            }
        });

        final EditText NumberOfData = (EditText) findViewById(R.id.numberOfDataEditText);
        NumberOfData.setText(numberOfData[0]);
        final EditText NumberOfPacket = (EditText) findViewById(R.id.numberOfPacketEditText);
        NumberOfPacket.setText(numberOfPacket[0]);
        final EditText SettleTime = (EditText) findViewById(R.id.settleTimeEditText);
        SettleTime.setText(settleTime[0]);
        final EditText TelemetryStart = (EditText) findViewById(R.id.telemetryStartEditText);
        TelemetryStart.setText(telemetryStart[0]);
        final EditText TailInclude = (EditText) findViewById(R.id.tailIncludeEditText);
        TailInclude.setText(tailInclude[0]);
        final EditText PacketHeader = (EditText) findViewById(R.id.packetHeaderEditText);
        PacketHeader.setText(packetHeader[0]);
        final EditText LeadingZeroLength = (EditText) findViewById(R.id.leadingZeroLengthEditText);
        LeadingZeroLength.setText(leadingZeroLength[0]);
        final EditText NumberOfRepetition = (EditText) findViewById(R.id.numberOfRepetitionEditText);
        NumberOfRepetition.setText(numberOfRepetition[0]);
        final EditText GapLength = (EditText) findViewById(R.id.gapLengthEditText);
        GapLength.setText(gapLength[0]);
        final EditText TriggerStart = (EditText) findViewById(R.id.triggerStartEditText);
        TriggerStart.setText(triggerStart[0]);
        final EditText TriggerStop = (EditText) findViewById(R.id.triggerStopEditText);
        TriggerStop.setText(triggerStop[0]);
        final EditText TriggerPermanent = (EditText) findViewById(R.id.triggerPermanentEditText);
        TriggerPermanent.setText(triggerPermanent[0]);
        final EditText TriggerSelect = (EditText) findViewById(R.id.triggerSelectEditText);
        TriggerSelect.setText(triggerSelect[0]);
        final EditText TimeStampDelay = (EditText) findViewById(R.id.timeStampDelayEditText);
        TimeStampDelay.setText(timeStampDelay[0]);
        final EditText AFEcontrol = (EditText) findViewById(R.id.AFEcontrolEditText);
        AFEcontrol.setText(afeControl[0]);
        final EditText SimulatorTimeStamp = (EditText) findViewById(R.id.simulatorTimeStampEditText);
        SimulatorTimeStamp.setText(simulatorTimeStamp[0]);
        final EditText MUXhead = (EditText) findViewById(R.id.MUXheadEditText);
        MUXhead.setText(muxHead[0]);
        final EditText ADCclock = (EditText) findViewById(R.id.ADCclockEditText);
        ADCclock.setText(adcClock[0]);
        final EditText TriggerClock = (EditText) findViewById(R.id.triggerClockEditText);
        TriggerClock.setText(triggerClock[0]);
        final EditText MUXclock = (EditText) findViewById(R.id.MUXclockEditText);
        MUXclock.setText(muxClock[0]);

        Spinner ADCenable = (Spinner) findViewById(R.id.ADCenableSpinner);
        ArrayAdapter<CharSequence> ADCenableAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.ADCenableArray, R.layout.global_settings_spinner_item);
        ADCenableAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        ADCenable.setAdapter(ADCenableAdapter);
        ADCenable.setSelection(adcEnablePosition[0]);
        ADCenable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adcEnablePosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner MUXmode = (Spinner) findViewById(R.id.MUXmodeSpinner);
        ArrayAdapter<CharSequence> MUXmodeAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.MUXmodeArray, R.layout.global_settings_spinner_item);
        MUXmodeAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        MUXmode.setAdapter(MUXmodeAdapter);
        MUXmode.setSelection(muxModePosition[0]);
        MUXmode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                muxModePosition[0] = position;
                switch (position) {
                    case 0:
                        MAX_GRAPHS = 0;
                        break;
                    case 1:
                        MAX_GRAPHS = 16;
                        break;
                    case 2:
                    case 3:
                        MAX_GRAPHS = 8;
                        break;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        MAX_GRAPHS = 4;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner MUXstart = (Spinner) findViewById(R.id.MUXstartSpinner);
        ArrayAdapter<CharSequence> MUXstartAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.MUXstartArray, R.layout.global_settings_spinner_item);
        MUXstartAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        MUXstart.setAdapter(MUXstartAdapter);
        MUXstart.setSelection(muxStartPosition[0]);
        MUXstart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                muxStartPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner MUXheadDelay = (Spinner) findViewById(R.id.MUXheadDelaySpinner);
        ArrayAdapter<CharSequence> MUXheadDelayAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.MUXheadDelayArray, R.layout.global_settings_spinner_item);
        MUXheadDelayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        MUXheadDelay.setAdapter(MUXheadDelayAdapter);
        MUXheadDelay.setSelection(muxHeadDelayPosition[0]);
        MUXheadDelay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                muxHeadDelayPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner MUXselect = (Spinner) findViewById(R.id.MUXselectSpinner);
        ArrayAdapter<CharSequence> MUXselectAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.MUXselectArray, R.layout.global_settings_spinner_item);
        MUXselectAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        MUXselect.setAdapter(MUXselectAdapter);
        MUXselect.setSelection(muxSelectPosition[0]);
        MUXselect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                muxSelectPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner ClockDiv = (Spinner) findViewById(R.id.clockDivSpinner);
        ArrayAdapter<CharSequence> ClockDivAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.ClockDivArray, R.layout.global_settings_spinner_item);
        ClockDivAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        ClockDiv.setAdapter(ClockDivAdapter);
        ClockDiv.setSelection(clockDivPosition[0]);
        ClockDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clockDivPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner Reset = (Spinner) findViewById(R.id.resetSpinner);
        ArrayAdapter<CharSequence> resetAdapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.ResetArray, R.layout.global_settings_spinner_item);
        resetAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        Reset.setAdapter(resetAdapter);
        Reset.setSelection(resetPosition[0]);
        Reset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                resetPosition[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button TelemetryControlButton = (Button) findViewById(R.id.telemetryControlUpdateSettingsButton);
        TelemetryControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button TriggerControlButton = (Button) findViewById(R.id.triggerControlUpdateSettingsButton);
        TriggerControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button ADCcontrolButton = (Button) findViewById(R.id.ADCcontrolUpdateSettingsButton);
        ADCcontrolButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button MUXcontrolButton = (Button) findViewById(R.id.MUXcontrolUpdateSettingsButton);
        MUXcontrolButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button AFEcontrolButton = (Button) findViewById(R.id.AFEcontrolUpdateSettingsButton);
        AFEcontrolButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button SystemControlButton = (Button) findViewById(R.id.systemControlUpdateSettingsButton);
        SystemControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button PolarityControlButton = (Button) findViewById(R.id.polarityControlUpdateSettingsButton);
        PolarityControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkNecsisEditTextErrors()) {
                    String message = getNecsisFirstSevenIntegersString();
                    String temp = "" + resetPosition[0] + muxSelectPosition[0] + muxStartPosition[0] + "000" +
                            triggerSelect[0] + "000" + adcEnablePosition[0] + "00" + tailInclude[0] +
                            telemetryStart[0] + "0" + triggerPermanent[0] + triggerStop[0] + triggerStart[0];
                    temp = binaryToUnsignedDecimal(temp) + ";";
                    message+=temp + " ";
                    sendNecsisCommand(message);
                }
            }
        });
        Button SendGlobalConfigButton = (Button) findViewById(R.id.sendGlobalConfigButton);
        SendGlobalConfigButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command = "n";
                command+="g";
                command+=" ";
                sendNecsisCommand(command);
            }
        });
        Button SendLocalConfigButton = (Button) findViewById(R.id.sendLocalConfigButton);
        SendLocalConfigButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command = "n";
                command+="l";
                command+=" ";
                sendNecsisCommand(command);
            }
        });
        Button SendMUXresetButton = (Button) findViewById(R.id.sendMUXresetConfigButton);
        SendMUXresetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "69632;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendMUXtelemetryStartButton = (Button) findViewById(R.id.sendMUXtelemetryStartConfigButton);
        SendMUXtelemetryStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "4112;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendMUXtelemetryStartStopButton = (Button) findViewById(R.id.sendMUXtelemetryStartStopConfigButton);
        SendMUXtelemetryStartStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "4112;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendMUXstartButton = (Button) findViewById(R.id.sendMUXstartConfigButton);
        SendMUXstartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "4113;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendTriggerStart1100Button = (Button) findViewById(R.id.sendTriggerStart1100ConfigButton);
        SendTriggerStart1100Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "69632;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendTriggerStop1100Button = (Button) findViewById(R.id.sendTriggerStop1100ConfigButton);
        SendTriggerStop1100Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "4098;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendMUXstart1100Button = (Button) findViewById(R.id.sendMUXstart1100ConfigButton);
        SendMUXstart1100Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "69632;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });
        Button SendSystemReset1100Button = (Button) findViewById(R.id.sendSystemReset1100ConfigButton);
        SendSystemReset1100Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = getNecsisFirstSevenIntegersString();
                String temp = "266240;";
                message+=temp + " ";
                sendNecsisCommand(message);
            }
        });

    }

    private void showWaveform() {
        screen = "waveform";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.waveform, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // Find, at most, 4 different types of channel settings
        ArrayList<MyXYSeries> list = new ArrayList<>();
        for (int different = 0, channelIndex = 0;
             different < 4 && channelIndex < channels.length; channelIndex++) {
            ChannelSettings channel = channels[channelIndex];
            if (channel.isSet()) {
                boolean same = false;
                for (int i = 0; i < list.size(); i++) {
                    if (channel.sameSettingsAs(channels[list.get(i).getID() - 1])) {
                        same = true;
                        break;
                    }
                }
                if (!same) {
                    different++;
                    list.add(new MyXYSeries(channelIndex + 1));
                }
            }
        }

        LinearLayout layout = (LinearLayout) dialoglayout.findViewById(R.id.waveformLayout);
        // Add points to the series's and draw them
        for (MyXYSeries series : list) {
            // Create constants
            ChannelSettings channel = channels[series.getID() - 1];
            double aAmpValue = Double.parseDouble(ampArray[channel.getaAmpPosition()]);
            double cAmpValue = Double.parseDouble(ampArray[channel.getcAmpPosition()]);
            double aPhaseWidthValue = Double.parseDouble(widthArray[channel.getaPhaseWidthPosition()]);
            double cPhaseWidthValue = Double.parseDouble(widthArray[channel.getcPhaseWidthPosition()]);
            double startDelayValue = Double.parseDouble(getResources().getStringArray(
                    R.array.startDelayArray)[channel.getStartDelayPosition()]);
            double interPhaseDelayValue = Double.parseDouble(interphaseArray[channel.getInterPhaseDelayPosition()]);
            boolean reverse = channel.isReversePolarity();

            // Add points
            series.add(0, 0);
            series.add(startDelayValue, 0);
            if (reverse) {
                series.add(startDelayValue, aAmpValue);
                series.add(startDelayValue + aPhaseWidthValue, aAmpValue);
                series.add(startDelayValue + aPhaseWidthValue, 0);
                series.add(startDelayValue + aPhaseWidthValue + interPhaseDelayValue, 0);
                series.add(startDelayValue + aPhaseWidthValue + interPhaseDelayValue, -cAmpValue);
                series.add(startDelayValue + aPhaseWidthValue + interPhaseDelayValue
                        + cPhaseWidthValue, -cAmpValue);
            } else {
                series.add(startDelayValue, -cAmpValue);
                series.add(startDelayValue + cPhaseWidthValue, -cAmpValue);
                series.add(startDelayValue + cPhaseWidthValue, 0);
                series.add(startDelayValue + cPhaseWidthValue + interPhaseDelayValue, 0);
                series.add(startDelayValue + cPhaseWidthValue + interPhaseDelayValue, aAmpValue);
                series.add(startDelayValue + cPhaseWidthValue + interPhaseDelayValue
                        + aPhaseWidthValue, aAmpValue);
            }

            XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
            dataset.addSeries(series);

            // Create data point renderer
            XYSeriesRenderer renderer = new XYSeriesRenderer();
            renderer.setLineWidth(3);
            renderer.setColor(Color.RED);
            // Include low and max value
            renderer.setDisplayBoundingPoints(true);

            // Renderer that controls the full charts
            XYMultipleSeriesRenderer seriesRenderer = new XYMultipleSeriesRenderer();
            seriesRenderer.addSeriesRenderer(renderer);
            seriesRenderer.setBackgroundColor(Color.WHITE);
            // Cannot control axes of the graphs
            seriesRenderer.setZoomEnabled(false, false);
            seriesRenderer.setPanEnabled(false, false);
            seriesRenderer.setChartTitle("Channel " + series.getID());
            seriesRenderer.setXTitle("ms");
            seriesRenderer.setShowGrid(true);
            seriesRenderer.setGridColor(Color.GRAY);
            seriesRenderer.setMarginsColor(Color.WHITE);
            seriesRenderer.setXLabelsColor(Color.BLACK);
            seriesRenderer.setYLabelsColor(0, Color.BLACK);
            seriesRenderer.setLabelsColor(Color.BLACK);

            // Add graph to layout and graphs array
            GraphicalView graphicalView = ChartFactory.getLineChartView(this, dataset, seriesRenderer);
            graphicalView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, 600));
            graphicalView.setBackgroundColor(Color.WHITE);
            // Prevent focusing which leads to crashing of graph
            graphicalView.setFocusable(false);
            layout.addView(graphicalView);
        }

        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setCustomMessageScreen() {
        screen = "custom_message";
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.custom_message, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        final EditText editText = (EditText) dialoglayout.findViewById(R.id.customMessage);

        builder.setView(dialoglayout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String text = editText.getText().toString();
                alertDialog.dismiss();
                sendCustomMessage(text);
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        switch (screen) {
            case "set_channels":
            case "global_settings":
            case "recording":
            case "presets_list":
            case "necsis":
            case "preset_save":
            case "network":
            case "switches":
            case "waveform":
            case "delays":
            case "custom_message":
                setMainMenuScreen();
                break;
            case "preset_load":
                setPresetsListScreen();
                break;
        }
        hideKeyboard();
    }

    public void deselectAllChannels() {
        for (final ChannelSettings channel : channels) {
            channel.setSelected(false);
        }
        setMainMenuScreen();
    }

    /**
     * Updates the arrays for amp, phase width, and interphase delay
     */
    public void updateArrays() {
        double maxPhaseWidthValue = Double.parseDouble(getResources().getStringArray(
                R.array.maxPhaseWidthArray)[maxPhaseWidthPosition[0]]);
        double maxPhaseAmpValue = Double.parseDouble(getResources().getStringArray(
                R.array.maxPhaseAmpArray)[maxPhaseAmpPosition[0]]);
        ampArray = new String[16];
        for (int i = 0; i < ampArray.length; i++) {
            ampArray[i] = "" + (Math.floor(i * maxPhaseAmpValue / (ampArray.length - 1) * 100) / 100);
        }
        widthArray = new String[8];
        for (int i = 0; i < widthArray.length; i++) {
            widthArray[i] = "" + (Math.floor((i + 1) * maxPhaseWidthValue / (widthArray.length) * 10000) / 10000);
        }
        interphaseArray = new String[8];
        for (int i = 0; i < interphaseArray.length; i++) {
            interphaseArray[i] = "" + (i * maxPhaseWidthValue / interphaseArray.length);
        }
    }

    /**
     * Starts the Threads which send the string and receive and record the string
     */
    private void sendMessage() {
        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this,
                "Establishing Connection", "", true);

        final Thread thread = new Thread(new Runnable() {

            final Socket[] socket = {null};
            final DataInputStream[] dataInputStream = {null};
            final DataOutputStream[] dataOutputStream = {null};
            boolean connected = false;

            private final long CONNECTION_TIMEOUT_MILIS = 1000;

            /**
             * Attempts connection to the specified Socket 10 times
             *
             * @param startTime The initial start time of the method. Used for recursive purposes
             * @return {@code true} if successful, {@code false} otherwise
             */
            boolean connectToSocket(long startTime) {
                if (System.currentTimeMillis() - startTime < CONNECTION_TIMEOUT_MILIS) {
                    try {
                        synchronized (socket) {
                            socket[0] = new Socket(ipAddress, portNumber);
                            dataInputStream[0] = new DataInputStream(socket[0].getInputStream());
                            dataOutputStream[0] = new DataOutputStream(socket[0].getOutputStream());
                        }
                        return true;
                    } catch (ConnectException | NullPointerException e) {
                        e.printStackTrace();
                        return connectToSocket(startTime);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                } else {
                    return false;
                }
            }

            final String getMessage() {
                //The messages that will be sent to the server
                String globalMessage = "";
                String localMessage = "";

                String periodDesiredValue = getResources().getStringArray(
                        R.array.periodDesiredArray)[periodDesiredPosition[0]];
                int maxPhaseWidthValue = Integer.parseInt(getResources().getStringArray(
                        R.array.maxPhaseWidthArray)[maxPhaseWidthPosition[0]]) - 1;
                int maxPhaseAmpValue = Integer.parseInt(getResources().getStringArray(
                        R.array.maxPhaseAmpArray)[maxPhaseAmpPosition[0]]) - 1;
                int numOfPulsesValue = Integer.parseInt(getResources().getStringArray(
                        R.array.numOfPulsesArray)[numOfPulsesPosition[0]]);

                //Append strings for global message
                globalMessage += periodDesiredValue + ";";
                globalMessage += maxPhaseAmpValue + ";";
                globalMessage += maxPhaseWidthValue + ";";
                globalMessage += triggerOutWidthPosition[0] + ";";

                //Append strings for each channel
                for (int i = 0; i < channels.length; i++) {
                    final ChannelSettings channel = channels[i];
                    if (channel.isSet()) {
                        localMessage += i + ";";
                        localMessage += channel.getaAmpPosition() + ";";
                        localMessage += channel.getcAmpPosition() + ";";
                        localMessage += channel.getaPhaseWidthPosition() + ";";
                        localMessage += channel.getcPhaseWidthPosition() + ";";
                        localMessage += channel.getStartDelayPosition() + ";";
                        localMessage += channel.getInterPhaseDelayPosition() + ";";
                        localMessage += channel.isReversePolarity() ? 1 : 0;
                        localMessage += ";";
                        localMessage += channel.isChargeCancellation() ? 1 : 0;
                        localMessage += ";";
                    }
                }

                String temp = "00" + periodDesiredValue;
                temp = temp.substring(temp.length() - 2, temp.length());


                String temp2 = "00" + phaseDelayValue[0];
                temp2 = temp2.substring(temp2.length() - 2, temp2.length());

                String temp3 = "00" + numOfPulsesValue;
                temp3.substring(temp3.length() - 2, temp2.length());

                return globalMessage + localMessage + " " + temp + " " +  temp2 + " " + contStimBoolean[0] + " " + temp3;
            }

            final Thread sendingDataThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (socket) {
                        try {
                            String message = getMessage();
                            if (testing)
                                dataOutputStream[0].writeUTF(message);
                            else
                                dataOutputStream[0].writeBytes(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            final Thread receivingDataThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    final String[] receivedText = {""};
                    double dx = 1.0 / 500;
                    double max_value = Math.pow(2, 10) - 1;
                    final int BIT_LENGTH = 12;
                    int numChannels = MAX_GRAPHS;
                    int channelStart = 0;
                    switch (muxModePosition[0]) {
                        case 0:
                        case 1:
                        case 2:
                        case 4:
                            channelStart = 0;
                            break;
                        case 5:
                            channelStart = 4;
                            break;
                        case 3:
                        case 6:
                            channelStart = 8;
                            break;
                        case 7:
                            channelStart = 12;
                            break;
                    }
                    try {
                        if (numChannels != 0) {
                            // Allows the graph to visually update periodically
                            long updateTimer = 0;

                            long initialTime = System.currentTimeMillis();
                            int channel = 0;

                            synchronized (dataInputStream) {
                                while (sending[0]) {

                                    byte b = dataInputStream[0].readByte();
                                    String s = Integer.toBinaryString(toUnsignedByte(b));
                                    s = "00000000" + s;
                                    s = s.substring(s.length() - 8, s.length());
                                    receivedText[0] += s;

                                    if (receivedText[0].length() >= BIT_LENGTH) {

                                        String temp = receivedText[0].substring(0, BIT_LENGTH);

                                        int stimFlag = binaryToUnsignedDecimal(temp.substring(BIT_LENGTH - 2, BIT_LENGTH - 1));
                                        int value = binaryToUnsignedDecimal(temp.substring(0, BIT_LENGTH - 2));

                                        receivedText[0] = receivedText[0].substring(BIT_LENGTH, receivedText[0].length());

                                        double yValue = value / max_value;
                                        boolean stimulated = stimFlag == 1;

                                        addDataToSeriesArray(channel + channelStart, dx, yValue, stimulated);
                                        channel++;
                                        channel %= numChannels;


                                        // Visually update the graph at a particular rate
                                        if (System.currentTimeMillis() - updateTimer >=
                                                1000 / MainActivity.GRAPH_UPDATE_RATE) {
                                            updateTimer = System.currentTimeMillis();
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    for (GraphicalView graph : graphs) {
                                                        graph.repaint();
//                                              Log.v("Graph", "Repainting");
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        dataInputStream[0].close();
                        socket[0].close();
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (StringIndexOutOfBoundsException e) {
                    }
                }
            });

            @Override
            public void run() {
                progressDialog.show();
                connected = connectToSocket(System.currentTimeMillis());
                progressDialog.dismiss();
                try {
                    if (connected) {
                        sendingDataThread.start();
                        if (expectingReceivingData[0])
                            receivingDataThread.start();
                    } else {
                        dataInputStream[0].close();
                        dataOutputStream[0].close();
                        socket[0].close();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sending[0] = false;
                                setMainMenuScreen();
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                AlertDialog dialog = builder.create();
                                dialog.setMessage("Failed to Connect to Server!");
                                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                dialog.show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    /**
     * Sends the stop signal to the MCU
     */
    private void sendStop() {
        final String stopSignal = "x ";
        new Thread(new Runnable() {
            private final long CONNECTION_TIMEOUT_MILIS = 8000;
            /**
             * Attempts connection to the specified Socket 10 times
             * @param startTime The initial start time of the method. Used for recursive purposes
             * @return {@code true} if successful, {@code false} otherwise
             */
            boolean connectToSocket(long startTime) {
                if (System.currentTimeMillis() - startTime < CONNECTION_TIMEOUT_MILIS) {
                    try {
                        socket = new Socket(ipAddress, portNumber);
                        dataOutputStream = new DataOutputStream(socket.getOutputStream());
                        return true;
                    } catch (ConnectException | NullPointerException e) {
                        e.printStackTrace();
                        return connectToSocket(startTime);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                } else {
                    return false;
                }
            }

            @Override
            public void run() {
                connectToSocket(System.currentTimeMillis());
                try {
                    if (testing)
                        dataOutputStream.writeUTF(stopSignal);
                    else
                        dataOutputStream.writeBytes(stopSignal);
                    dataOutputStream.close();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    /**
     * Sends a custom message
     * @param message the custom message to be sent
     */
    private void sendCustomMessage(final String message) {
        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this,
                "Establishing Connection", "", true);

        final Thread thread = new Thread(new Runnable() {

            final Socket[] socket = {null};
            final DataInputStream[] dataInputStream = {null};
            final DataOutputStream[] dataOutputStream = {null};
            boolean connected = false;

            private final long CONNECTION_TIMEOUT_MILIS = 1000;

            /**
             * Attempts connection to the specified Socket 10 times
             *
             * @param startTime The initial start time of the method. Used for recursive purposes
             * @return {@code true} if successful, {@code false} otherwise
             */
            boolean connectToSocket(long startTime) {
                if (System.currentTimeMillis() - startTime < CONNECTION_TIMEOUT_MILIS) {
                    try {
                        synchronized (socket) {
                            socket[0] = new Socket(ipAddress, portNumber);
                            dataInputStream[0] = new DataInputStream(socket[0].getInputStream());
                            dataOutputStream[0] = new DataOutputStream(socket[0].getOutputStream());
                        }
                        return true;
                    } catch (ConnectException | NullPointerException e) {
                        e.printStackTrace();
                        return connectToSocket(startTime);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                } else {
                    return false;
                }
            }

            final Thread sendingDataThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (socket) {
                        try {
                            if (testing)
                                dataOutputStream[0].writeUTF(message);
                            else
                                dataOutputStream[0].writeBytes(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            @Override
            public void run() {
                progressDialog.show();
                connected = connectToSocket(System.currentTimeMillis());
                progressDialog.dismiss();
                try {
                    if (connected) {
                        sendingDataThread.start();
                    } else {
                        dataInputStream[0].close();
                        dataOutputStream[0].close();
                        socket[0].close();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sending[0] = false;
                                setMainMenuScreen();
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                AlertDialog dialog = builder.create();
                                dialog.setMessage("Failed to Connect to Server!");
                                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                dialog.show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    /**
     * Resets the xySeriesArrayList, graphs, and seriesRenderers and adds the currently
     * selected channels to the xySeriesArrayList
     */
    private void setUpGraphs() {
        int[] channelNumbers = {};
        switch (muxModePosition[0]) {
            case 0:
                MAX_GRAPHS = 0;
                break;
            case 1: {
                MAX_GRAPHS = 16;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1);
                }
                break;
            }
            case 2: {
                MAX_GRAPHS = 8;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1);
                }
                break;
            }
            case 3: {
                MAX_GRAPHS = 8;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1) + 8;
                }
                break;
            }
            case 4: {
                MAX_GRAPHS = 4;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1);
                }
                break;
            }
            case 5: {
                MAX_GRAPHS = 4;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1) + 4;
                }
                break;
            }
            case 6: {
                MAX_GRAPHS = 4;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1) + 8;
                }
                break;
            }
            case 7: {
                MAX_GRAPHS = 4;
                channelNumbers = new int[MAX_GRAPHS];
                for (int i = 0; i < MAX_GRAPHS; i++) {
                    channelNumbers[i] = (i + 1) + 12;
                }
                break;
            }
        }
        xySeriesArrayList = new ArrayList<>();
        graphs = new ArrayList<>();
        seriesRenderers = new ArrayList<>();
        for (int i = 0; i < MAX_GRAPHS; i++) {
            MyXYSeries series = new MyXYSeries(channelNumbers[i]);
            // Creates the XYSeries
            xySeriesArrayList.add(series);
            XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
            dataset.addSeries(series);

            // Create data point renderer
            XYSeriesRenderer renderer = new XYSeriesRenderer();
            renderer.setLineWidth(2);
            renderer.setColor(Color.RED);
            // Include low and max value
            renderer.setDisplayBoundingPoints(true);
            // Add point markers
            renderer.setPointStyle(PointStyle.CIRCLE);
            renderer.setPointStrokeWidth(3);

            // Renderer that controls the full charts
            XYMultipleSeriesRenderer seriesRenderer = new XYMultipleSeriesRenderer();
            seriesRenderer.addSeriesRenderer(renderer);
            seriesRenderer.setBackgroundColor(Color.WHITE);
            // Cannot control axes of the graphs
            seriesRenderer.setZoomEnabled(false, false);
            seriesRenderer.setPanEnabled(false, false);
            // Aesthetics
            seriesRenderer.setChartTitle(series.getTitle());
            seriesRenderer.setXTitle("seconds");
            seriesRenderer.setShowGrid(true);
            seriesRenderer.setGridColor(Color.GRAY);
            seriesRenderer.setMarginsColor(Color.WHITE);
            seriesRenderer.setXLabelsColor(Color.BLACK);
            seriesRenderer.setYLabelsColor(0, Color.BLACK);
            seriesRenderer.setLabelsColor(Color.BLACK);
            seriesRenderers.add(seriesRenderer);
        }
    }

    /**
     * Adds the specified data point to the series array and to writes to the .txt file
     * @param ID The graph ID of the data point
     * @param dx The dx-value of the data points
     * @param y The y-value of the data point
     * @param stimulated {@code true} if the channel is stimulated, {@code false} otherwise
     */
    private void addDataToSeriesArray(int ID, double dx, double y, boolean stimulated) {
        double xRange = MyXYSeries.VISIBLE_WINDOW;
        for (int i = 0; i < xySeriesArrayList.size(); i++) {
            if(xySeriesArrayList.get(i).getID() - 1 == ID) {
                int points = xySeriesArrayList.get(i).getTotalItemCount();
                double x = dx*(points + 1);
                xySeriesArrayList.get(i).add(x, y);
                if (screen.equals("recording")) {
                    seriesRenderers.get(i).setXAxisMax(x);
                    seriesRenderers.get(i).setXAxisMin(x - xRange);
                    seriesRenderers.get(i).setYAxisMax(xySeriesArrayList.get(i).getMaxY());
                    seriesRenderers.get(i).setYAxisMin(xySeriesArrayList.get(i).getMinY());
                }
                // Write data point to .txt file
                try {
                    String s = ID + "\t";
                    s+= stimulated ? "Y" : "N";
                    s+= "\t" + x + "\t\t\t" + y;
                    FileWriter fileWriter = new FileWriter(dataFilePath + (ID+1) + ".txt", true);
                    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                    PrintWriter printWriter = new PrintWriter(bufferedWriter);
                    printWriter.println(s);
//                    Log.v("File", "Wrote to " + (i + 1) + ": Channel " + channelNumber);
                    printWriter.close();
                    bufferedWriter.close();
                    fileWriter.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Log.v("Data x", "" + x);
//                Log.v("Data Points", "" + points);
//                Log.v("Data dx", "" + dx);
                return;
            }
        }
    }

    /**
     * Sends all text files containing the data points from the graphs to the
     * specified email address.
     * @param emailAddress The email address to receive the text files
     * @return {@code true} if finished with no errors, {@code false} otherwise
     */
    private boolean sendDataPoints(String emailAddress) {
        try {
            Intent send = new Intent();
            send.setAction(Intent.ACTION_SEND_MULTIPLE);

            send.setType("message/rfc822");
            send.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});

            ArrayList<Uri> uris = new ArrayList<>();

            for (int i = 0; i < MAX_GRAPHS; i++) {
                File file = new File(dataFilePath + (i+1) + ".txt");
                Uri uri = Uri.fromFile(file);
                uris.add(uri);
            }

            send.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            startActivity(Intent.createChooser(send, "Email: Text File"));

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Clears all files for data points storage and sets up data table
     */
    private boolean resetTextDataFile() {
        boolean newFile = true;
        for (int i = 0; i < MAX_GRAPHS; i++) {
            File file = new File(dataFilePath + (i+1) + ".txt");
            try {
                newFile = file.createNewFile();
                PrintWriter out = new PrintWriter(file);
                String s = "ID/#\tStim.\tx\t\t\ty";
                out.println(s);
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return newFile;
    }

    /**
     * Sends the NECSIS command settings
     * @param command the String of commands to send
     */
    private void sendNecsisCommand(final String command) {
        hideKeyboard();
        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this,
                "Establishing Connection", "", true);
        new Thread(new Runnable() {

            private final long CONNECTION_TIMEOUT_MILIS = 8000;
            /**
             * Attempts connection to the specified Socket 10 times
             * @param startTime The initial start time of the method. Used for recursive purposes
             * @return {@code true} if successful, {@code false} otherwise
             */
            boolean connectToSocket(long startTime) {
                if (System.currentTimeMillis() - startTime < CONNECTION_TIMEOUT_MILIS) {
                    try {
                        socket = new Socket(ipAddress, portNumber);
                        dataOutputStream = new DataOutputStream(socket.getOutputStream());
                        dataInputStream = new DataInputStream(socket.getInputStream());
                        return true;
                    } catch (ConnectException | NullPointerException e) {
                        e.printStackTrace();
                        return connectToSocket(startTime);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                } else {
                    return false;
                }
            }

            @Override
            public void run() {
                try {
                    progressDialog.show();
                    boolean connected = connectToSocket(System.currentTimeMillis());
                    progressDialog.dismiss();
                    if (connected) {
                        if (testing)
                            dataOutputStream.writeUTF(command);
                        else
                            dataOutputStream.writeBytes(command);
                        socket.close();
                        dataOutputStream.close();
                        dataInputStream.close();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                AlertDialog dialog = builder.create();
                                dialog.setMessage("Failed to Connect to Server!");
                                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        sendNecsisCommand(command);
                                    }
                                });
                                dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "SETTINGS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        setNetworkScreen();
                                        dialog.dismiss();
                                    }
                                });
                                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Sends the bytes for the switches
     */
    private void sendSwitches() {
        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this,
                "Establishing Connection", "", true);
        new Thread(new Runnable() {

            private final long CONNECTION_TIMEOUT_MILIS = 8000;
            /**
             * Attempts connection to the specified Socket 10 times
             * @param startTime The initial start time of the method. Used for recursive purposes
             * @return {@code true} if successful, {@code false} otherwise
             */
            boolean connectToSocket(long startTime) {
                if (System.currentTimeMillis() - startTime < CONNECTION_TIMEOUT_MILIS) {
                    try {
                        socket = new Socket(ipAddress, portNumber);
                        dataOutputStream = new DataOutputStream(socket.getOutputStream());
                        dataInputStream = new DataInputStream(socket.getInputStream());
                        return true;
                    } catch (ConnectException | NullPointerException e) {
                        e.printStackTrace();
                        return connectToSocket(startTime);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                } else {
                    return false;
                }
            }

            @Override
            public void run() {
                try {
                    progressDialog.show();
                    boolean connected = connectToSocket(System.currentTimeMillis());
                    progressDialog.dismiss();
                    if (connected) {
                        byte first = (byte) 'c';

                        byte second = 0;
                        if (pin62boolean[0])
                            second|=0b10000000;
                        if (pin61boolean[0])
                            second|=0b1000000;
                        if (pin60boolean[0])
                            second|=0b100000;
                        if (pin03boolean[0])
                            second|=0b10000;
                        if (pin58boolean[0])
                            second|=0b10000;
                        if (pin59boolean[0])
                            second|=0b100;
                        if (pin64boolean[0])
                            second|=0b10;
                        if (pin63boolean[0])
                            second|=0b1;

                        byte third = 0;
                        if (pin16boolean[0])
                            third|=0b10000000;
                        if (pin15boolean[0])
                            third|=0b1000000;
                        if (pin18boolean[0])
                            third|=0b10000;
                        if (pin17boolean[0])
                            third|=0b1;

                        byte fourth = 0;
                        if (pin45boolean[0])
                            fourth|=0b10000000;
                        if (pin53boolean[0])
                            fourth|=0b1000000;
                        if (pin04boolean[0])
                            fourth|=0b100000;
                        if (pin50boolean[0])
                            fourth|=0b1;

                        byte[] message = {first, second, third, fourth};

                        if (testing)
                            dataOutputStream.writeUTF(first + ";" + second + ";" + third + ";" + fourth);
                        else
                            dataOutputStream.write(message);
                        socket.close();
                        dataOutputStream.close();
                        dataInputStream.close();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                AlertDialog dialog = builder.create();
                                dialog.setMessage("Failed to Connect to Server!");
                                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sendSwitches();
                                        dialog.dismiss();
                                    }
                                });
                                dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "SETTINGS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        setNetworkScreen();
                                        dialog.dismiss();
                                    }
                                });
                                dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Checks for errors in entered values for the EditTexts on the NECSIS screen
     * @return {@code true} if all values satisfy the conditions for sending
     */
    private boolean checkNecsisEditTextErrors() {
        final EditText NumberOfData = (EditText) findViewById(R.id.numberOfDataEditText);
        final EditText NumberOfPacket = (EditText) findViewById(R.id.numberOfPacketEditText);
        final EditText SettleTime = (EditText) findViewById(R.id.settleTimeEditText);
        final EditText TelemetryStart = (EditText) findViewById(R.id.telemetryStartEditText);
        final EditText TailInclude = (EditText) findViewById(R.id.tailIncludeEditText);
        final EditText PacketHeader = (EditText) findViewById(R.id.packetHeaderEditText);
        final EditText LeadingZeroLength = (EditText) findViewById(R.id.leadingZeroLengthEditText);
        final EditText NumberOfRepetition = (EditText) findViewById(R.id.numberOfRepetitionEditText);
        final EditText GapLength = (EditText) findViewById(R.id.gapLengthEditText);
        final EditText TriggerStart = (EditText) findViewById(R.id.triggerStartEditText);
        final EditText TriggerStop = (EditText) findViewById(R.id.triggerStopEditText);
        final EditText TriggerPermanent = (EditText) findViewById(R.id.triggerPermanentEditText);
        final EditText TriggerSelect = (EditText) findViewById(R.id.triggerSelectEditText);
        final EditText TimeStampDelay = (EditText) findViewById(R.id.timeStampDelayEditText);
        final EditText AFEcontrol = (EditText) findViewById(R.id.AFEcontrolEditText);
        final EditText SimulatorTimeStamp = (EditText) findViewById(R.id.simulatorTimeStampEditText);
        final EditText MUXhead = (EditText) findViewById(R.id.MUXheadEditText);
        final EditText ADCclock = (EditText) findViewById(R.id.ADCclockEditText);
        final EditText TriggerClock = (EditText) findViewById(R.id.triggerClockEditText);
        final EditText MUXclock = (EditText) findViewById(R.id.MUXclockEditText);

        boolean sendable = true;
        boolean isEmpty = NumberOfData.getText().toString().equals("");
        long data = isEmpty ? -1 : Long.parseLong(NumberOfData.getText().toString());
        if (data < 0 || data > (long) Math.pow(2, 32) - 1) {
            sendable = false;
            NumberOfData.setError("Value must be an integer in the range [0, 2^32)!");
        }
        isEmpty = NumberOfPacket.getText().toString().equals("");
        int packet = isEmpty ? -1 : Integer.parseInt(NumberOfPacket.getText().toString());
        if (packet < 0 || packet > 63) {
            sendable = false;
            NumberOfPacket.setError("Value must be an integer in the range 0-63!");
        }
        isEmpty = SettleTime.getText().toString().equals("");
        int settle = isEmpty ? -1 : Integer.parseInt(SettleTime.getText().toString());
        if (settle < 0 || settle > 63) {
            sendable = false;
            SettleTime.setError("Value must be an integer in the range 0-63!");
        }
        isEmpty = TelemetryStart.getText().toString().equals("");
        int start = isEmpty ? -1 : Integer.parseInt(TelemetryStart.getText().toString());
        if (start < 0 || start > 1) {
            sendable = false;
            TelemetryStart.setError("Value must be either a 0 or 1!");
        }
        isEmpty = TailInclude.getText().toString().equals("");
        int tail = isEmpty ? -1 : Integer.parseInt(TailInclude.getText().toString());
        if (tail < 0 || tail > 1) {
            sendable = false;
            TailInclude.setError("Value must be either a 0 or 1!");
        }
        isEmpty = PacketHeader.getText().toString().equals("");
        int header = isEmpty ? -1 : Integer.parseInt(PacketHeader.getText().toString());
        if (header < 0 || header > (int) Math.pow(2, 12) - 1) {
            sendable = false;
            PacketHeader.setError("Value must be an integer in the range [0, 2^12)!");
        }
        isEmpty = LeadingZeroLength.getText().toString().equals("");
        int zero = isEmpty ? -1 : Integer.parseInt(LeadingZeroLength.getText().toString());
        if (zero < 0 || zero > (int) Math.pow(2, 24) - 1) {
            sendable = false;
            LeadingZeroLength.setError("Value must be an integer in the range [0, 2^24)!");
        }
        isEmpty = NumberOfRepetition.getText().toString().equals("");
        int repetition = isEmpty ? -1 : Integer.parseInt(NumberOfRepetition.getText().toString());
        if (repetition < 0 || repetition > 255) {
            sendable = false;
            NumberOfRepetition.setError("Value must be an integer in the range 0-255!");
        }
        isEmpty = GapLength.getText().toString().equals("");
        int gap = isEmpty ? -1 : Integer.parseInt(GapLength.getText().toString());
        if (gap < 0 || gap > 15) {
            sendable = false;
            GapLength.setError("Value must be an integer in the range 0-15!");
        }
        isEmpty = TriggerStart.getText().toString().equals("");
        int start2 = isEmpty ? -1 : Integer.parseInt(TriggerStart.getText().toString());
        if (start2 < 0 || start2 > 1) {
            sendable = false;
            TriggerStart.setError("Value must be either a 0 or 1!");
        }
        isEmpty = TriggerStop.getText().toString().equals("");
        int stop = isEmpty ? -1 : Integer.parseInt(TriggerStop.getText().toString());
        if (stop < 0 || stop > 1) {
            sendable = false;
            TriggerStop.setError("Value must be either a 0 or 1!");
        }
        isEmpty = TriggerPermanent.getText().toString().equals("");
        int permanent = isEmpty ? -1 : Integer.parseInt(TriggerPermanent.getText().toString());
        if (permanent < 0 || permanent > 1) {
            sendable = false;
            TriggerPermanent.setError("Value must be either a 0 or 1!");
        }
        isEmpty = TriggerSelect.getText().toString().equals("");
        int select = isEmpty ? -1 : Integer.parseInt(TriggerSelect.getText().toString());
        if (select < 0 || select > 1) {
            sendable = false;
            TriggerSelect.setError("Value must be either a 0 or 1!");
        }
        isEmpty = TimeStampDelay.getText().toString().equals("");
        int delay = isEmpty ? -1 : Integer.parseInt(TimeStampDelay.getText().toString());
        if (delay < 0 || delay > 15) {
            sendable = false;
            TimeStampDelay.setError("Value must be an integer in the range 0-15");
        }
        isEmpty = AFEcontrol.getText().toString().equals("");
        int i = isEmpty ? -1 : Integer.parseInt(AFEcontrol.getText().toString());
        if (i < 0 || i > 63) {
            sendable = false;
            AFEcontrol.setError("Value must be an integer in the range 0-63!");
        }
        isEmpty = SimulatorTimeStamp.getText().toString().equals("");
        int stamp = isEmpty ? -1 : Integer.parseInt(SimulatorTimeStamp.getText().toString());
        if (stamp < 0 || stamp > 1) {
            sendable = false;
            SimulatorTimeStamp.setError("Value must be either a 0 or 1!");
        }
        isEmpty = MUXhead.getText().toString().equals("");
        int head = isEmpty ? -1 : Integer.parseInt(MUXhead.getText().toString());
        if (head < 0 || head > 1) {
            sendable = false;
            MUXhead.setError("Value must be either a 0 or 1!");
        }
        isEmpty = ADCclock.getText().toString().equals("");
        int adc = isEmpty ? -1 : Integer.parseInt(ADCclock.getText().toString());
        if (adc < 0 || adc > 1) {
            sendable = false;
            ADCclock.setError("Value must be either a 0 or 1!");
        }
        isEmpty = TriggerClock.getText().toString().equals("");
        int trigger = isEmpty ? -1 : Integer.parseInt(TriggerClock.getText().toString());
        if (trigger < 0 || trigger > 1) {
            sendable = false;
            TriggerClock.setError("Value must be either a 0 or 1!");
        }
        isEmpty = MUXclock.getText().toString().equals("");
        int mux = isEmpty ? -1 : Integer.parseInt(MUXclock.getText().toString());
        if (mux < 0 || mux > 1) {
            sendable = false;
            MUXclock.setError("Value must be either a 0 or 1!");
        }

        return sendable;
    }

    /**
     * Gets the first 7 integers of the NECSIS string to be sent, starting with {@code "n"} and
     * ending with {@code ";"}.
     * Also, updates the EditText's of all fields on the NECSIS screen.
     * @return the String to be sent as a message
     */
    private String getNecsisFirstSevenIntegersString() {

        final EditText NumberOfData = (EditText) findViewById(R.id.numberOfDataEditText);
        final EditText NumberOfPacket = (EditText) findViewById(R.id.numberOfPacketEditText);
        final EditText SettleTime = (EditText) findViewById(R.id.settleTimeEditText);
        final EditText TelemetryStart = (EditText) findViewById(R.id.telemetryStartEditText);
        final EditText TailInclude = (EditText) findViewById(R.id.tailIncludeEditText);
        final EditText PacketHeader = (EditText) findViewById(R.id.packetHeaderEditText);
        final EditText LeadingZeroLength = (EditText) findViewById(R.id.leadingZeroLengthEditText);
        final EditText NumberOfRepetition = (EditText) findViewById(R.id.numberOfRepetitionEditText);
        final EditText GapLength = (EditText) findViewById(R.id.gapLengthEditText);
        final EditText TriggerStart = (EditText) findViewById(R.id.triggerStartEditText);
        final EditText TriggerStop = (EditText) findViewById(R.id.triggerStopEditText);
        final EditText TriggerPermanent = (EditText) findViewById(R.id.triggerPermanentEditText);
        final EditText TriggerSelect = (EditText) findViewById(R.id.triggerSelectEditText);
        final EditText TimeStampDelay = (EditText) findViewById(R.id.timeStampDelayEditText);
        final EditText AFEcontrol = (EditText) findViewById(R.id.AFEcontrolEditText);
        final EditText SimulatorTimeStamp = (EditText) findViewById(R.id.simulatorTimeStampEditText);
        final EditText MUXhead = (EditText) findViewById(R.id.MUXheadEditText);
        final EditText ADCclock = (EditText) findViewById(R.id.ADCclockEditText);
        final EditText TriggerClock = (EditText) findViewById(R.id.triggerClockEditText);
        final EditText MUXclock = (EditText) findViewById(R.id.MUXclockEditText);

        numberOfData[0] = NumberOfData.getText().toString();
        numberOfPacket[0] = NumberOfPacket.getText().toString();
        settleTime[0] = SettleTime.getText().toString();
        telemetryStart[0] = TelemetryStart.getText().toString();
        tailInclude[0] = TailInclude.getText().toString();
        packetHeader[0] = PacketHeader.getText().toString();
        leadingZeroLength[0] = LeadingZeroLength.getText().toString();
        numberOfRepetition[0] = NumberOfRepetition.getText().toString();
        gapLength[0] = GapLength.getText().toString();
        triggerStart[0] = TriggerStart.getText().toString();
        triggerStop[0] = TriggerStop.getText().toString();
        triggerPermanent[0] = TriggerPermanent.getText().toString();
        triggerSelect[0] = TriggerSelect.getText().toString();
        timeStampDelay[0] = TimeStampDelay.getText().toString();
        afeControl[0] = AFEcontrol.getText().toString();
        simulatorTimeStamp[0] = SimulatorTimeStamp.getText().toString();
        muxHead[0] = MUXhead.getText().toString();
        adcClock[0] = ADCclock.getText().toString();
        triggerClock[0] = TriggerClock.getText().toString();
        muxClock[0] = MUXclock.getText().toString();

        char s = 'n';

        String first = leadingZeroLength[0] + ";";

        String second = "";
        String temp = "0000";
        temp+=decimalToBinary(Integer.parseInt(gapLength[0]));
        temp = temp.substring(temp.length() - 4, temp.length());
        String temp2 = "00000000";
        temp2+=decimalToBinary(Integer.parseInt(numberOfRepetition[0]));
        temp2 = temp2.substring(temp2.length() - 8, temp2.length());
        second+=temp + temp2 + "00000";
        second = binaryToUnsignedDecimal(second) + ";";

        String third = numberOfData[0] + ";";

        String fourth = "";
        temp = "000000";
        temp+=decimalToBinary(Integer.parseInt(numberOfPacket[0]));
        temp = temp.substring(temp.length() - 6, temp.length());
        fourth+=temp + "0000000000000";
        fourth = binaryToUnsignedDecimal(fourth) + ";";

        String fifth = "";
        temp = "000000";
        temp+=decimalToBinary(Integer.parseInt(afeControl[0]));
        temp = temp.substring(temp.length() - 6, temp.length());
        temp2 = "000000";
        temp2+=decimalToBinary(Integer.parseInt(settleTime[0]));
        temp2 = temp2.substring(temp2.length() - 6, temp2.length());
        fifth+=temp + "00" + temp2;
        fifth = binaryToUnsignedDecimal(fifth) + ";";

        String sixth = "";
        temp = "0000";
        temp+=decimalToBinary(muxHeadDelayPosition[0]);
        temp = temp.substring(temp.length() - 4, temp.length());
        temp2 = "000";
        temp2+=decimalToBinary(muxModePosition[0]);
        temp2 = temp2.substring(temp2.length() - 3, temp2.length());
        String temp3 = "000000";
        temp3+=decimalToBinary(clockDivPosition[0]);
        temp3 = temp3.substring(temp3.length() - 6, temp3.length());
        sixth+=muxClock[0] + triggerClock[0] + adcClock[0] + muxHead[0] + simulatorTimeStamp[0] +
                temp + temp2 + temp3;
        sixth = binaryToUnsignedDecimal(sixth) + ";";

        String seventh = "";
        temp = "0000";
        temp+=decimalToBinary(Integer.parseInt(timeStampDelay[0]));
        temp = temp.substring(temp.length() - 4, temp.length());
        temp2 = "000000000000";
        temp2+=decimalToBinary(Integer.parseInt(packetHeader[0]));
        temp2 = temp2.substring(temp2.length() - 12, temp2.length());
        seventh+= temp + temp2;
        seventh = binaryToUnsignedDecimal(seventh) + ";";

        return s + first + second + third + fourth + fifth + sixth + seventh;
    }

    /**
     * Forces the closing/hiding of the on-screen keyboard
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * @param type "0" if the WifiConfiguration is WPA2 Enterprise. Defaults
     *             to WPA/WPA2 Personal
     * @return {@code true} if the activity has connected to the SSID
     */
    private boolean ConfigureAndConnectToWifi(int type) {
        Log.v("Network", "Configuring");
        WifiConfiguration conf = new WifiConfiguration();
        conf.status = WifiConfiguration.Status.ENABLED;
        conf.SSID = "\"" + MainActivity.networkSSID + "\"";
        if (type == 0) {
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);
            conf.enterpriseConfig.setPassword(MainActivity.networkEnterprisePassword);
            conf.enterpriseConfig.setEapMethod(WifiEnterpriseConfig.Eap.PEAP);
            conf.enterpriseConfig.setPhase2Method(WifiEnterpriseConfig.Phase2.MSCHAPV2);
            conf.enterpriseConfig.setIdentity(MainActivity.networkEnterpriseUsername);
            Log.v("Network", "Enterprise");
        } else {
            conf.preSharedKey = "\"" + MainActivity.networkPersonalPassword + "\"";
            conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            Log.v("Network", "Personal");
        }

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        //Clear wifimanager to prevent errors in connecting
        for (int i = 0; i < wifiManager.getConfiguredNetworks().size(); i++) {
            boolean removed = wifiManager.removeNetwork(i);
            //Log.v("Network", removed + " : " + i);
            i = removed ? i - 1 : i;
        }

        int netID = wifiManager.addNetwork(conf);

        boolean connected = false;
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        Log.v("Network", "List size: " + list.size());
        for (WifiConfiguration i : list) {
            int netID2 = i.networkId;
            Log.v("Network", netID2 + " : " + i.SSID);
        }

        Log.v("Network", "netID: " + netID);

        if (netID != -1) {
            wifiManager.disconnect();
            wifiManager.enableNetwork(netID, true);
            wifiManager.reconnect();
            ConnectivityManager cm =
                    (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = null;
            Log.wtf("Receiver", "Working");
            //Timeout for connecting to WIFI
            for(long i = System.currentTimeMillis(); System.currentTimeMillis() - i <
                    MainActivity.WIFI_TIMEOUT_MILIS && !connected; ){
                activeNetwork = cm.getActiveNetworkInfo();
                connected = (activeNetwork != null) && activeNetwork.isConnected();
            }
            Log.wtf("Receiver", "" + connected);
            if(!connected && (activeNetwork != null))
                Log.wtf("Receiver", activeNetwork.getReason());
        }

        return connected;
    }

    /**
     * Converts the String of binary digits into an unsigned integer.
     * @param s The String of binary digits
     * @return the unsigned integer representation of the binary String
     */
    public static int binaryToUnsignedDecimal(String s) {
        int sum = 0;
        int p = s.length() - 1;
        for (int i = 0; i < s.length(); i++, p--) {
            int temp = Integer.parseInt(s.substring(i, i+1));
            sum+=(temp*(Math.pow(2, p)));
        }
        return sum;
    }

    /**
     * Converts the signed byte into an unsigned integer data type.
     * The byte is converted by simply adding 128 which is the absolute
     * value of the minimum value of a Java byte data type.
     * @param signedByte The signed value of the byte
     * @return The unsigned value of the byte as an integer
     */
    public static int toUnsignedByte(byte signedByte){
        return (byte) (signedByte & 0xFF);
    }

    /**
     * Converts the integer decimal into a binary string
     * @param i The integer decimal to be converted to binary string
     * @return The binary representation of an integer decimal
     */
    public static String decimalToBinary(int i) {
        String s = "";
        int length = 0;
        while(i / ((int) Math.pow(2, length)) != 0)
            length++;
        length--;
        for(int j = length; j > 0; j--) {
            int temp = i/((int) Math.pow(2, j));
            s+=temp;
            i%=((int) Math.pow(2, j));
        }
        s+=i;
        return s;
    }

}
