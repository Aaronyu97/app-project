package aaron.wifiwithsockets;

import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by Aaron on 1/29/17.
 */

class Preset {

    public String name;
    int phaseShiftPosition;
    int periodDesiredPosition;
    int maxPhaseWidthPosition;
    int maxPhaseAmpPosition;
    int triggerOutWidthPosition;
    int numOfPulsesPosition;
    boolean contStimBoolean;
    SettingList localSettings;

    Preset() {
        phaseShiftPosition = 0;
        periodDesiredPosition = 0;
        maxPhaseWidthPosition = 0;
        maxPhaseAmpPosition = 0;
        triggerOutWidthPosition = 0;
        numOfPulsesPosition = 0;
        contStimBoolean = false;
        localSettings = new SettingList();
    }

    @SuppressWarnings("WeakerAccess")
    class SettingList {
        private boolean set;
        private ArrayList<Settings> list = new ArrayList<>();
        public ArrayList<Settings> getList() { return  list; }
        public boolean add(Settings s) { return list.add(s); }
        public boolean isSet() { return set; }
        public void set() { set = true; }
    }

    void addLocalSetting(ChannelSettings setting) {
        Settings s = new Settings();
        s.channelNumber = setting.getChannelID();
        s.aAmpPosition = setting.getaAmpPosition();
        s.cAmpPosition = setting.getcAmpPosition();
        s.aPhaseWidthPosition = setting.getaPhaseWidthPosition();
        s.cPhaseWidthPosition = setting.getcPhaseWidthPosition();
        s.startDelayPosition = setting.getStartDelayPosition();
        s.interPhaseDelayPosition = setting.getInterPhaseDelayPosition();
        s.reversePolarity = setting.isReversePolarity();
        s.chargeCancellation = setting.isChargeCancellation();
        localSettings.add(s);
    }

    class Settings {
        int channelNumber;
        int aAmpPosition;
        int cAmpPosition;
        int aPhaseWidthPosition;
        int cPhaseWidthPosition;
        int startDelayPosition;
        int interPhaseDelayPosition;
        boolean reversePolarity;
        boolean chargeCancellation;
    }

    static final String FILE_EXISTS = "FILE EXISTS!";
    static final String SUCCESS = "SUCCESS!";
    static final String ERROR = "ERROR!";
    private static final String PRESET_FOLDER = "PRESET";

    static boolean makeDirectory(String dataFileDirectory) {
        dataFileDirectory = dataFileDirectory + File.separator + PRESET_FOLDER;
        File dir = new File(dataFileDirectory);
        Log.v("Directory", "Already Exists: " + dir.exists());
        Log.v("Directory", "Path: " + dir.getAbsolutePath());
        return dir.mkdir();
    }

    private static final char GROUP_MARKER = '$';

    String saveToFile(String filePath) {
        File file = new File(filePath + PRESET_FOLDER + File.separator + name + ".txt");
        Log.v("Preset.saveToFile", file.getAbsolutePath());
        try {
            boolean exists = !file.createNewFile();
            if (exists) {
                return FILE_EXISTS;
            } else {
                PrintWriter out = new PrintWriter(file);
                String s = "name:" + this.name + ";";
                s += ":" + GROUP_MARKER + ";";
                s += "periodDesiredPosition:" + this.periodDesiredPosition + ";";
                s += "maxPhaseWidthPosition:" + this.maxPhaseWidthPosition + ";";
                s += "maxPhaseAmpPosition:" + this.maxPhaseAmpPosition + ";";
                s += "triggerOutWidthPosition:" + this.triggerOutWidthPosition + ";";
                s += "numOfPulsesPosition:" + this.numOfPulsesPosition + ";";
                s += "contStimBoolean:";
                s += this.contStimBoolean ? 1 : 0;
                s += ";";
                s += "phaseShiftPosition:" + this.phaseShiftPosition + ";";
                for (Settings setting : localSettings.getList()) {
                    s += "channelNumber:" + setting.channelNumber + ";";
                    s += "aAmpPosition:" + setting.aAmpPosition + ";";
                    s += "cAmpPosition:" + setting.cAmpPosition + ";";
                    s += "aPhaseWidthPosition:" + setting.aPhaseWidthPosition + ";";
                    s += "cPhaseWidthPosition:" + setting.cPhaseWidthPosition + ";";
                    s += "startDelayPosition:" + setting.startDelayPosition + ";";
                    s += "interPhaseDelayPosition:" + setting.interPhaseDelayPosition + ";";
                    s += "reversePolarity:";
                    s += setting.reversePolarity ? 1 : 0;
                    s += ";";
                    s += "chargeCancellation:";
                    s += setting.chargeCancellation ? 1 : 0;
                    s += ";";
                }
                out.print(s);
                out.close();
                return SUCCESS;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    static ArrayList<Preset> getPresets(String dataFileDirectory) {
        dataFileDirectory = dataFileDirectory + PRESET_FOLDER;
        ArrayList<Preset> presets = new ArrayList<>();

        File dir = new File(dataFileDirectory);
//        Log.v("Presets", "" + dir.exists());
        File[] list = dir.listFiles();
//        Log.v("Presets", "" + list.length);
        if (list != null) {
            for (File preset : list) {
                presets.add(getPresetFromTextFile(preset.getAbsolutePath()));
            }
        }
//        Log.v("Presets", "" + presets.size());
        return presets;
    }

    @SuppressWarnings("unused")
    static void deleteAllPresets(String dataFileDirectory) {
        dataFileDirectory = dataFileDirectory + PRESET_FOLDER;
        File dir = new File(dataFileDirectory);
        File[] list = dir.listFiles();
        if (list != null) {
            for (File preset : list) {
                preset.delete();
            }
        }
    }

    private static Preset getPresetFromTextFile(String fileName) {
        String s;
        try {
            s = readFile(fileName);
            Log.v("Preset String", s);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Preset preset = new Preset();
        int colon = s.indexOf(':');
        int semicolon = s.indexOf(';');

        // Global Settings
        preset.name = s.substring(colon + 1, semicolon);
        int[] globalSettings = new int[6];
        for (int i = 0; i < globalSettings.length; i++) {
            colon = s.indexOf(':', colon + 1);
            semicolon = s.indexOf(';', semicolon + 1);
            globalSettings[i] = Integer.parseInt(s.substring(colon + 1, semicolon));
        }
        preset.periodDesiredPosition = globalSettings[0];
        preset.maxPhaseWidthPosition = globalSettings[1];
        preset.maxPhaseAmpPosition = globalSettings[2];
        preset.triggerOutWidthPosition = globalSettings[3];
        preset.numOfPulsesPosition = globalSettings[4];
        preset.contStimBoolean = globalSettings[5] == 1;
        preset.phaseShiftPosition = globalSettings[6];

        // Local Settings
        while (semicolon + 1 < s.length()) {
            int[] temp = new int[9];
            for (int j = 0; j < temp.length; j++) {
                colon = s.indexOf(':', colon + 1);
                semicolon = s.indexOf(';', semicolon + 1);
                temp[j] = Integer.parseInt(s.substring(colon + 1, semicolon));
            }
            preset.addSetting(temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6],
                    temp[7] == 1, temp[8] == 1);
        }

        return preset;
    }

    private void addSetting(int channelNumber, int aAmpPosition, int cAmpPosition,
                            int aPhaseWidthPosition, int cPhaseWidthPosition,
                            int startDelayPosition, int interPhaseDelayPosition,
                            boolean reversePolarity, boolean chargeCancellation) {
        Settings s = new Settings();
        s.channelNumber = channelNumber;
        s.aAmpPosition = aAmpPosition;
        s.cAmpPosition = cAmpPosition;
        s.aPhaseWidthPosition = aPhaseWidthPosition;
        s.cPhaseWidthPosition = cPhaseWidthPosition;
        s.startDelayPosition = startDelayPosition;
        s.interPhaseDelayPosition = interPhaseDelayPosition;
        s.reversePolarity = reversePolarity;
        s.chargeCancellation = chargeCancellation;
        localSettings.add(s);
    }

    private static String readFile(String file) throws IOException {
        String         line;
        StringBuilder  stringBuilder = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        }
    }

    static boolean deletePreset(String filePath, Preset preset) {
        File file = new File(filePath + File.separator + PRESET_FOLDER + File.separator + preset.name + ".txt");
        return file.delete();
    }

}

