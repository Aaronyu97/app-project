package aaron.wifiwithsockets;

import org.achartengine.model.XYSeries;

/**
 * Created by Aaron on 3/1/17.
 */

class MyXYSeries extends XYSeries {

    private int ID;
    private boolean stimulated;

    static final int VISIBLE_WINDOW = 1;
    private static final int SAMPLE_RATE = 500;
    private static final int MAX_POINTS_DISPLAYED = VISIBLE_WINDOW*SAMPLE_RATE;
    private int totalItemCount;

    MyXYSeries(int ID) {
        super("Channel " + ID);
        this.ID = ID;
        totalItemCount = 0;
    }

    int getID() { return ID; }

    public void setStimulated(boolean b) { stimulated = b; }

    public boolean isStimulated() { return stimulated; }

    @Override
    public synchronized void add(double x, double y) {
        super.add(x, y);
        totalItemCount++;
        if (this.getItemCount() > MAX_POINTS_DISPLAYED)
            this.remove(0);
    }

    int getTotalItemCount() { return totalItemCount; }
}
